#include <iostream>
#include <string>

using namespace std;

/* actual_index, j, k, consumed, type, sigma, form, sent/(child, child) */

/* Forms:
 * a*, x = 0, y = 1 // a*
 * a+, x = 1, y = 1 // a+
 * a^k, x = k, y = - 1 // a^2
 * a^k(a^j)*, x = k, y = j // a^2(a^n)*
 * a^k(a^j)+, x = k + j, y = j // a^2(a^n)+
 */

enum REGEX_FORM {
    STAR = 0, 
    PLUS,
    FIXED,
    FIXED_STAR,
    FIXED_PLUS
};

class regex_parser {
    public:
    // returns the "consumed" amount of spikes
    bool solve(int form, int j, int k, int c) {
		if (c == 0) {
			return false;
		}

        if (form == FIXED) {
            return c - k == 0;
        }
        else if (form == STAR) {
            //return 0 % 1;
			return c % 1 == 0;
        }
        else if (form == PLUS) {
            //return 1 % 1;
			return c - 1 % 1 == 0;
        }
        else if (form == FIXED_STAR) {
            //return k % j;
			return c - k % j == 0;
        }
        else if (form == FIXED_PLUS) {
            //return (k + j) % j;
			return c - (k + j) % j == 0;
        }
        else {
            cout << "Invalid form requested!" << endl;
			return false;
        }
    }
    private:
};
