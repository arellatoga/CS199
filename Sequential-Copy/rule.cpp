#include "rule.hpp"

rule::rule (int _j, int _k, int _consumed, int _type, int _sigma, int _form) {
	j = _j;
	k = _k;
	consumed = _consumed;
	type = _type;
	sigma = _sigma;
	form = _form;
}

rule::rule (int _j, int _k, int _consumed, int _type, int _sigma, int _form, int _sent) {
	j = _j;
	k = _k;
	consumed = _consumed;
	type = _type;
	sigma = _sigma;
	form = _form;
	sent = _sent;
}

rule::rule (int _j, int _k, int _consumed, int _type, int _sigma, int _form, int _child1, int _child2) {
	j = _j;
	k = _k;
	consumed = _consumed;
	type = _type;
	sigma = _sigma;
	form = _form;
	child1 = _child1;
	child2 = _child2;
}
