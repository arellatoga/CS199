/*
 * CS 199
 * gahl
 * 2013-14806
 */

#include <iostream>
#include <string>
#include <vector>
#include <utility>

// Boost libraries
// Matrices
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>
// Tokenizer
#include <boost/tokenizer.hpp>

// personal libraries
#include "rule.hpp"
#include "neuron.hpp"
#include "synapse.hpp"
#include "regex.cpp"

/* Regex Forms:
 * a^*, x = 0, y = 1
 * a^+, x = 1, y = 1
 * a^k, x = k, y = - 1
 * a^k(a^j)^*, x = k, y = j
 * a^k(a^j)^+, x = k + j, y = j
 */

/*
 * Rule representation construct:
 * r_i = {E, j, d', c}
 * r_i = {x, y, j, d', c, in, form}
 * j, k - see Regex Forms above
 * j - the name of the neuron
 * in - actual index of said neuron
 * d' - fire state
 * c - consumed spikes
 */

// To save a few keystrokes
using namespace std;
using namespace boost::numeric::ublas;

// All the matrices involved
matrix<int> configVector;
matrix<int> netGainVector;

// Gain Vector and its components
matrix<int> gainVector;
matrix<int> indicatorVector;
matrix<int> transitionMatrix;
matrix<int> reductionMatrix;

// Loss Vector and its components
matrix<int> lossVector; // m
matrix<int> spikingVector; // m
matrix<int> ruleMatrix;

// Other vectors
matrix<int> statusVector; // m
matrix<int> maskVector; // n
matrix<int> trainVector; // m
matrix<int> divisionVector;
matrix<int> ruleRepresentation;
matrix<int> spikeTrainVector;
matrix<int> delayVector;

// regex parser
regex_parser regexParser;

// dictionaries, etc
std::vector<neuron> neurons;
std::vector<rule> rules;

// function prototypes
void simulate_snp();
matrix<int> matrix_elementwise_mult(matrix<int>, matrix<int>);
matrix<int> reset(matrix<int>);
matrix<int> reset_opposite(matrix<int>);
matrix<int> compute_spiking_vector();
matrix<int> multiply_vectors(matrix<int>, matrix<int>);
void read_input();

int iterations;

enum ruleType {
    FIRE = 0,
    FORGET,
    DIVIDE,
    DISSOLVE
};

int main () {
	read_input();
	cout << "CV: " << configVector << endl << endl;
	for (int i = 0; i < iterations; i++) {
		simulate_snp();
	}
    cout << "Program terminated" << endl;
}

void simulate_snp() {
    matrix<int> oldMaskVector = maskVector;
    // Get the rules which are supposed to be division or dissolution
    matrix<int> checkVector = matrix_elementwise_mult(spikingVector, maskVector);
    for (int i = 0; i < checkVector.size2(); ++i) {
        if (checkVector (0, i) == 1) {
            // divide
            //checkVector (0, i) = 0;
        }
        else if (checkVector (0, i) == -1) {
            // Dissolve
        }
    }

    maskVector = oldMaskVector;
    lossVector = reset(lossVector);
    gainVector = reset(gainVector);
    netGainVector = reset(netGainVector);
    indicatorVector = reset(indicatorVector);
    compute_spiking_vector();
	// rule rep = {j, k, sigma, d', c, in}
    
    cout << "D: " << delayVector << endl;
    cout << "D': ";
	
    for (int i = 0; i < ruleRepresentation.size1(); ++i) {
        cout << ruleRepresentation(i, 3) << " ";
        int neuron_index = ruleRepresentation(i, 2);

        if (spikingVector(0, i) == 1) {
			lossVector (0, neuron_index) = ruleRepresentation(i, 4);
			ruleRepresentation(i, 3) = delayVector(0, i);
			indicatorVector(0, i) = 0;

			if(ruleRepresentation(i, 3) == 0) {
				indicatorVector(0, i) = 1;
				statusVector(0, neuron_index) = 1;
			}
        }
		else if (ruleRepresentation(i, 3) == 0) {
			indicatorVector(0, i) = 1;
			statusVector(0, neuron_index) = 1;
		}
    }

    cout << endl;

	// compute for Gain Vector
	gainVector = multiply_vectors(indicatorVector, transitionMatrix);
	netGainVector = matrix_elementwise_mult(gainVector, statusVector) - lossVector;
	configVector = configVector + netGainVector;

	/*
	cout << "TM: " << transitionMatrix << endl;
	cout << "NG: " << netGainVector << endl;
	*/
	cout << "IV: " << indicatorVector << endl;
	cout << "SV: " << spikingVector << endl;
	cout << "GV: " << gainVector << endl;
	cout << "LV: " << lossVector << endl;
	cout << "CV: " << configVector << endl << endl;

	for (int i = 0; i < ruleRepresentation.size1(); ++i) {
		if (ruleRepresentation(i, 3) != -1) {
			ruleRepresentation(i, 3)--;
		}
	}
}

matrix<int> reset(matrix<int> A) {
    for (int i = 0; i < A.size1(); ++i) {
        for (int j = 0; j < A.size2(); ++j) {
            A (i, j) = 0;
        }
    }

    return A;
}

matrix<int> reset_opposite(matrix<int> A) {
    for (int i = 0; i < A.size1(); ++i) {
        for (int j = 0; j < A.size2(); ++j) {
            A (i, j) = 1;
        }
    }

    return A;
}

matrix<int> matrix_elementwise_mult(matrix<int> A, matrix<int> B) {
    if (A.size1() != B.size1()) {
        std::cout << "Invalid row count!" << endl;
        return A;
    }
    else if (A.size2() != B.size2()) {
        std::cout << "Invalid column count!" << endl;
        return A;
    }

    matrix<int> returnMatrix (A.size1(), B.size2());

    //cout << "Size: " << A.size1() << " " << B.size2() << endl;

    // pre-increments for ~code of good taste~
    for (int i = 0; i < A.size1(); ++i) {
        for (int j = 0; j < A.size2(); ++j) {
            returnMatrix(i, j) = A(i, j) * B(i, j);
            //cout << returnMatrix(i, j) << endl;
        }
    }

    return returnMatrix;
}

matrix<int> compute_spiking_vector() {
    matrix <int> newSpikingVector (1, spikingVector.size2());
	// rule rep = {j, k, sigma, d', c, form}

    for (int i = 0; i < ruleRepresentation.size1(); i++) {
        // int l = neuron that contains the ith rule mentioned above.
		int l = ruleRepresentation (i, 2);

        if (statusVector(0, i) == 0) {
            newSpikingVector(0, l) = 0;
        }
        else {
			//cout << ruleRepresentation(0,5) << ruleRepresentation(0, 0) << ruleRepresentation(0,1) << endl;
			int sigma_spikes = configVector(0, l);
			bool does_spike = regexParser.solve(ruleRepresentation(i, 5), ruleRepresentation(i, 0), ruleRepresentation(i, 1), sigma_spikes);
            if (does_spike) {
                newSpikingVector(0, i) = 1;
            }
            else {
                newSpikingVector(0, i) = 0;
            }
        }
    }
	spikingVector = newSpikingVector;
	return newSpikingVector;
}

matrix<int> multiply_vectors(matrix<int> A, matrix<int> B) {
	matrix<int> result_vector;

	if (A.size2() != B.size1()) {
		cout << "Invalid matrices!" << endl;
		cout << "A row count: " << A.size2() << endl;
		cout << "B row count: " << B.size1() << endl;
		exit;
	}

	result_vector = matrix<int>(1, B.size2());

	for (int i = 0; i < B.size2(); ++i) {
		int sum = 0;
		for (int j = 0; j < A.size2(); ++j) {
			sum += A(0, j) * B(j, i);
		}

		result_vector(0, i) = sum;
	}

	return result_vector;
}

void read_input() {
	// local namespace use
	/*
	 * Order:
	 * Neurons (mode 0)
	 * Rules (mode 1)
	 * Synapses (mode 2)
	 * Spike trains (mode 3)
	 */
	using namespace boost;

	string string1;
	int mode = 0;
	int maxNeuronCount;
	int actualNeuronCount;
	int ruleCount;
	int ctr = 0;

	// get max neurons in system, the number of actual neurons, and rule count
	getline(cin, string1);
	tokenizer<> init(string1);
	tokenizer<>::iterator item = init.begin();
	maxNeuronCount = stoi(*(item++));
	actualNeuronCount = stoi(*(item++));
	ruleCount = stoi(*(item)++);
    iterations = stoi(*(item));

	matrix<int> synapses (maxNeuronCount, maxNeuronCount);
	statusVector = matrix<int> (1, actualNeuronCount);

	while (getline(cin, string1)) {
		tokenizer<> tok (string1);
		item = tok.begin();

		// cout << string1 << endl;

		if (string1[0] == '#') {
			// Comment line, skip this one
			continue;
		}
		else if (string1 == "NEXT") {
			mode++;
			ctr = 0;
			continue;
		}
		else if (string1 == "END") {
			break;
		}

		if (mode == 0) {
			/* label, spike */
			int label = stoi(*(item++));
			int spike = stoi(*(item));

			neuron new_neuron(label, spike);
			neurons.push_back(new_neuron);
		}
		else if (mode == 1) {
			/* j, k, consumed, type, sigma, form, sent/(child, child) */
			int j = stoi(*(item++));
			int k = stoi(*(item++));
			int c = stoi(*(item++));
			int type = stoi(*(item++));
			int sigma = stoi(*(item++));
			int form = stoi(*(item++));
			int spike = 0;
			int child1 = 0;
			int child2 = 0;
			
			/*
			 * type 0 - fire
			 * type 1 - forget 
			 * type 2 - divide
			 * type 3 - dissolve
			 */

			if (type == 0) {
				spike = stoi(*(item));

				rule new_rule(j, k, c, type, sigma, form, spike);
				rules.push_back(new_rule);
			}
			else if (type == 1 || type == 3) {
				rule new_rule(j, k, c, type, sigma, form);
				rules.push_back(new_rule);
			}
			else if (type == 2) {
				child1 = stoi(*(item++));
				child2 = stoi(*(item));

				rule new_rule(j, k, c, type, sigma, form, child1, child2);
				rules.push_back(new_rule);
			}
		}
		else if (mode == 2) {
			for (int i = 0; item != tok.end(); ++i, item++) {
				if (*item == "stop") {
					ctr++;
					break;
				}

				synapses(ctr, i) = stoi(*item);
			}
		}
		else if (mode == 3) {
			// none for now
		}
	}

	// Construct a transition matrix
	int actualRuleCount = 0;
	for (int i= 0; i < actualNeuronCount; i++) {
		int id = neurons[i].label;
		neurons[i].index = i;
		for (int j = 0; j < ruleCount; j++) {
			if (id == rules[j].sigma) {
				rules[j].actual_index = i;
				actualRuleCount++;
			}
		}
	}

	transitionMatrix = matrix<int>(ruleCount, maxNeuronCount);
	reductionMatrix = matrix<int>(ruleCount, maxNeuronCount);

	for (int i = 0; i < transitionMatrix.size1(); ++i) {
		for (int j = 0; j < transitionMatrix.size2(); ++j) {
			transitionMatrix(i, j) = 0;
			reductionMatrix(i, j) = 0;
		}
	}

	for (int i = 0; i < actualRuleCount; ++i) {
		for (int j = 0; j < maxNeuronCount; ++j) {
			if (neurons[j].spike != -1 &&
				synapses(rules[i].sigma, j) == 1 &&
				j != rules[i].sigma &&
				rules[i].type == 0) {
				transitionMatrix(i, j) = rules[i].sent;
			}
		}
	}

	for (int i = 0; i < actualRuleCount; ++i) {
		for (int j = 0; j < actualNeuronCount; ++j) {
			if (neurons[j].spike != -1 &&
				rules[i].sigma == j) {
				reductionMatrix(i, j) = rules[i].consumed;
			}
		}
	}

	int real_i = 0;
	configVector = matrix<int>(1, maxNeuronCount);
	for (int i = 0; i < maxNeuronCount; i++) {
		if (neurons[i].spike != -1) {
			configVector(0, real_i++) = neurons[i].spike;
		}
	}

	// rule rep = {j, k, sigma, d', c, form, sigma_actual}

	ruleRepresentation = matrix<int> (ruleCount, 7);

	lossVector = matrix<int> (1, maxNeuronCount);
	gainVector = matrix<int> (1, maxNeuronCount);
	netGainVector = matrix<int> (1, maxNeuronCount);
	indicatorVector = matrix<int> (1, ruleCount);

	maskVector = matrix<int> (1, ruleCount);
	spikingVector = matrix<int> (1, ruleCount);
	delayVector = matrix<int> (1, ruleCount);
	delayVector = reset(delayVector);
	
	for (int i = 0; i < ruleCount; i++) {
		ruleRepresentation(i, 0) = rules[i].j;
		ruleRepresentation(i, 1) = rules[i].k;
		ruleRepresentation(i, 2) = rules[i].sigma;
		ruleRepresentation(i, 3) = -1;
		ruleRepresentation(i, 4) = rules[i].consumed;
		ruleRepresentation(i, 5) = rules[i].form;
		ruleRepresentation(i, 6) = rules[i].sigma;
	}
	
	for (int i = 0; i < statusVector.size2(); i++) {
		statusVector(0, i) = 1;
	}

	cout << "C: " << configVector << endl;
	cout << "Synapses: " << synapses << endl;
	cout << "TM: " << transitionMatrix << endl;
	cout << "RM: " << reductionMatrix << endl;
	cout << "RR: " << ruleRepresentation << endl;
}
