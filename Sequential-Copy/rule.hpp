#pragma once

/*
 * Rule representation construct:
 * r_i = {E, j, d', c}
 * r_i = {j, k, n, d', c, in, form}
 * j, k - see Regex Forms above
 * n - the name of the neuron
 * in - actual index of said neuron
 * d' - fire state
 * c - consumed spikes
 * form - regex form
 */

/*
 * fire - 0
 * forget - 1
 * divide - 2
 * dissolve - 3
 */

/* j, k, consumed, type, sigma, form, sent/(child, child) */

class rule {
    public:
	rule (int, int, int, int, int, int);
	rule (int, int, int, int, int, int, int);
	rule (int, int, int, int, int, int, int, int);

	int actual_index;
	int j;
	int k;
	int consumed;
	int type;
	int sigma;
	int form;
	int sent;
	int child1;
	int child2;
};
