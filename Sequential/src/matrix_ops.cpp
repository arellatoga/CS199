#include "matrix_ops.hpp"

matrix<int> multiply_vectors(matrix<int> A, matrix<int> B) {
	matrix<int> result_vector;

	if (A.size2() != B.size1()) {
		cout << "Invalid matrices!" << endl;
		cout << "A column count: " << A.size2() << endl;
		cout << "B row count: " << B.size1() << endl;
		exit;
	}

	result_vector = matrix<int>(1, B.size2());

	for (int i = 0; i < B.size2(); ++i) {
		int sum = 0;
		for (int j = 0; j < A.size2(); ++j) {
			sum += A(0, j) * B(j, i);
		}

		result_vector(0, i) = sum;
	}

	return result_vector;
}

matrix<int> reset(matrix<int> A) {
    for (int i = 0; i < A.size1(); ++i) {
        for (int j = 0; j < A.size2(); ++j) {
            A (i, j) = 0;
        }
    }

    return A;
}

matrix<int> reset_opposite(matrix<int> A) {
    for (int i = 0; i < A.size1(); ++i) {
        for (int j = 0; j < A.size2(); ++j) {
            A (i, j) = 1;
        }
    }

    return A;
}

matrix<int> reset_negative(matrix<int> A) {
    matrix<int> new_A (A.size1(), A.size2());
    for (int i = 0; i < A.size1(); ++i) {
        //cout << i << endl;
        for (int j = 0; j < A.size2(); ++j) {
            //cout << "j: " << j;
            new_A (i, j) = -1;
        }
        //cout << endl;
    }
    //cout << "returning" << endl;

    return new_A;
}

matrix<int> matrix_elementwise_mult(matrix<int> A, matrix<int> B) {
    if (A.size1() != B.size1()) {
        std::cout << A.size1() << " " << B.size1() << endl;
        std::cout << "Invalid row count!" << endl;
        return A;
    }
    else if (A.size2() != B.size2()) {
        std::cout << A.size2() << " " << B.size2() << endl;
        std::cout << "Invalid column count!" << endl;
        return A;
    }

    matrix<int> returnMatrix (A.size1(), B.size2());

    //cout << "Size: " << A.size1() << " " << B.size2() << endl;

    // pre-increments for ~code of good taste~
    for (int i = 0; i < A.size1(); ++i) {
        for (int j = 0; j < A.size2(); ++j) {
            returnMatrix(i, j) = A(i, j) * B(i, j);
            //cout << returnMatrix(i, j) << endl;
        }
    }

    return returnMatrix;
}
