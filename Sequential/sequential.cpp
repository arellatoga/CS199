/*
 * CS 199
 * Simulator for Spiking Neural P Systems with Division and Dissolution
 * arel latoga
 * 2013-14806
 * /------\      /---\
 * |   a  | -->  |   |
 * | a->a |      |   |
 * \------/      \---/
 */

#include <iostream>
#include <string>
#include <vector>
#include <utility>

// Boost libraries
// Matrices
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>
// Tokenizer
#include <boost/tokenizer.hpp>

// personal libraries
#include "rule.hpp"
#include "neuron.hpp"
#include "synapse.hpp"
#include "regex.cpp"
#include "matrix_ops.hpp"
#include "train.hpp"

// To save a few keystrokes
using namespace std;
using namespace boost::numeric::ublas;

// All the matrices involved
matrix<int> configVector;
matrix<int> netGainVector;

// Gain Vector and its components
matrix<int> gainVector;
matrix<int> indicatorVector;
matrix<int> transitionMatrix;
matrix<int> reductionMatrix;

// Loss Vector and its components
matrix<int> lossVector; // m
matrix<int> spikingVector; // m

// Other vectors
matrix<int> statusVector; // m
matrix<int> maskVector; // n
matrix<int> trainVector; // m
matrix<int> divisionVector;
matrix<int> ruleRepresentation;
matrix<int> ruleRepresentationComplete; // containing all the rules
matrix<int> spikeTrainVector;
matrix<int> delayVector;
matrix<int> configIDVector;

// regex parser
regex_parser regexParser;

// dictionaries, etc
std::vector<neuron> neurons;
std::vector<rule> rules;
std::vector<spike_train> trains;
matrix<int> synapses;

// function prototypes
void simulate_snp();
matrix<int> compute_spiking_vector();
matrix<int> compute_spike_train();
int divide(int, int);
void dissolve(int, int);
void read_input();
void display_rr();
void display_cfg();

int iterations;
int timestep;

int maxNeuronCount; // number of possible neurons in system
int neuronCount; // actual number of neurons in the system
int ruleCount; // actual number of rules in the system
int maxRuleCount; // actual number of rules in the system
int maxMax;

int main () {
	read_input();
	//cout << "CV: " << configVector << endl << endl;
	for (timestep = 0; timestep < iterations; timestep++) {
        cout << "Step: " << timestep + 1<< endl;
		simulate_snp();
	}
    cout << "Program terminated" << endl;
}

void simulate_snp() {
    lossVector = reset(lossVector);
    gainVector = reset(gainVector);
    netGainVector = reset(netGainVector);
    indicatorVector = reset(indicatorVector);
    spikingVector = compute_spiking_vector();
    //cout << "SV0: " << spikingVector << endl;
    //cout << "MV: " << maskVector << endl;

    // Get the rules which are supposed to be division or dissolution
    matrix<int> checkVector = matrix_elementwise_mult(spikingVector, maskVector);
    //cout << ruleRepresentation << endl;
    //cout << "oDV: " << delayVector << endl;

    int offset = 0;
    for (int i = 0; i < checkVector.size2(); ++i) {
        if (checkVector (0, i) == 1) {
            checkVector (0, i) = 0;
            //cout << "Offset " << offset << endl;
            int add_offset = divide(i + offset, ruleRepresentation(i + offset, 6));
            if (add_offset > 0) add_offset--;
            offset += add_offset;
            //offset += divide(i + offset, ruleRepresentation(i + offset, 6)) - 1;
            //offset++; // skip the next one because it's literally the divided neuron
        }
        else if (checkVector (0, i) == -1) {
            dissolve(i + offset, ruleRepresentation(i + offset, 6));
        }
    }
 
	// rule rep = {j, k, sigma, d', c, form, sigma_actual}
    spikeTrainVector = compute_spike_train();
    for (int i = 0; i < ruleRepresentation.size1(); i++) {
        if (ruleRepresentation(i, 6) == -1) {
            continue;
        }

        int neuron_index = ruleRepresentation(i, 6);
        if (spikingVector(0, i) == 1) {
			lossVector (0, neuron_index) = ruleRepresentation(i, 4);
			ruleRepresentation(i, 3) = delayVector(0, i);
			indicatorVector(0, i) = 0;

			if (ruleRepresentation(i, 3) == 0) {
                //cout << "x";
				indicatorVector(0, i) = 1;
				statusVector(0, neuron_index) = 1;
			}
        }
		else if (ruleRepresentation(i, 3) == 0) {
			indicatorVector(0, i) = 1;
			statusVector(0, neuron_index) = 1;
		}
    }

	//cout << "SV: " << spikingVector << endl;
	//cout << "IV: " << indicatorVector << endl;
    //cout << "TM: " << transitionMatrix << endl;
    //cout << "RM: " << reductionMatrix << endl;
    //cout << "StV: " << statusVector << endl;

	// compute for Gain Vector
	gainVector = multiply_vectors(indicatorVector, transitionMatrix) + spikeTrainVector;
	netGainVector = matrix_elementwise_mult(gainVector, statusVector) - lossVector;
	configVector = configVector + netGainVector;

    
    //cout << "SpTV: " << spikeTrainVector << endl;
    //cout << "DV: " << delayVector << endl;
	//cout << "NG: " << netGainVector << endl;
	//cout << "GV: " << gainVector << endl;
	//cout << "LV: " << lossVector << endl;
    
	//cout << "CV: " << configVector << endl;
    //cout << "ID: " << configIDVector << endl << endl;
    display_cfg();

	for (int i = 0; i < ruleRepresentation.size1(); ++i) {
		if (ruleRepresentation(i, 3) != -1) {
			ruleRepresentation(i, 3)--;
		}
	}
    cout << endl << endl;
}

// 0-indexed index
void dissolve(int i_prime, int j_prime) {
    // These ones get reset: lossVector, gainVector, netGainVector, indicatorVector
    //cout << "Dissolved" << endl;
    //cout << "i', j': " << i_prime << " " << j_prime << endl;

    spikingVector(0, i_prime) = 0;
    maskVector(0, i_prime) = 0;
    delayVector(0, i_prime) = 0;
    statusVector(0, j_prime) = 0;
    configVector(0, j_prime) = 0;
    configIDVector(0, j_prime) = -1;

    int rulesRemoved = 0;
    
    for (int i = 0; i < maxRuleCount; i++) {
        if (ruleRepresentation(i, 6) == j_prime) {
            rulesRemoved++;
        }
    }

    //cout << "remove: " << rulesRemoved << endl;
    
 
    for (int i = 0; i < maxRuleCount; i++) {
        transitionMatrix(i, j_prime) = 0;
        reductionMatrix(i, j_prime) = 0;

        
        for (int ii = 0; i < ruleRepresentation.size1(); i++) {
            if (ruleRepresentation(0, 6) == j_prime) {
                transitionMatrix(ii, j_prime) = 0;
                reductionMatrix(ii, j_prime) = 0;
            }
        }
        

        /*
        if (ruleRepresentation(i, 6) == j_prime) {
            delayVector (0, i) = 0;
            for (int j = 0; j < ruleRepresentation.size2(); j++) {
                ruleRepresentation(i, j) = -1;
            }
        }
        */
    }   

    for (int i = i_prime; i < i_prime + rulesRemoved; i++) {
        if (ruleRepresentation(i, 6) == j_prime) {
            delayVector (0, i) = 0;
            for (int j = 0; j < ruleRepresentation.size2(); j++) {
                ruleRepresentation(i, j) = -1;
            }
        }
    }

    //cout << "TM: " << transitionMatrix << endl;
}

int divide(int i_prime, int j_prime) {
    //cout << "i', j': " << i_prime << " " << j_prime << endl;
    //Reset: GainVector, LossVector, IndicatorVector, NetGain
    spikingVector(0, i_prime) = 0;
    configVector(0, j_prime) = 0;
    configIDVector(0, j_prime) = 0;
    maskVector = reset(maskVector);

    matrix<int> spikingVectorTemp = reset(spikingVector);
    matrix<int> delayVectorTemp = reset(delayVector);
    matrix<int> ruleRepresentationTemp = reset_negative(ruleRepresentation);

    matrix<int> configVectorTemp = reset(configVector);
    matrix<int> configIDVectorTemp = reset_negative(configIDVector);
    matrix<int> statusVectorTemp = reset_opposite(statusVector);

    int rule_id = ruleRepresentation(i_prime, 7);
    int child_1 = rules[rule_id].child1;
    int child_2 = rules[rule_id].child2;

    neurons[child_1].spike = 0;
    neurons[child_2].spike = 0;
    neurons[j_prime].spike = 1;

    // Count the number of rules going to be added
    int neuronRuleAmount1 = 0;
    int neuronRuleAmount2 = 0;
    int newRuleCount = 0;
    for (int i = 0; i < rules.size(); i++) {
        if (rules[i].sigma == child_1) {
            neuronRuleAmount1++;
        }
        if (rules[i].sigma == child_2) {
            neuronRuleAmount2++;
        }
    }
    newRuleCount = neuronRuleAmount1 + neuronRuleAmount2;
    //cout << "newRuleCount: " << newRuleCount << endl;

    // Shift all rule-related stuff
    // If there are more than zero new rules, shift right
    if (newRuleCount > 0) {
        for (int i = 0; i < i_prime; i++) {
            spikingVectorTemp (0, i) = spikingVector(0, i);
            delayVectorTemp (0, i) = delayVector(0, i);

            for (int j = 0; j < ruleRepresentation.size2(); j++) {
                ruleRepresentationTemp(i, j) = ruleRepresentation(i, j);
            }
        }
        for (int i = i_prime + (newRuleCount-1); i < maxRuleCount; i++) {
            spikingVectorTemp (0, i) = spikingVector(0, i - (newRuleCount - 1));
            delayVectorTemp (0, i) = delayVector(0, i - (newRuleCount - 1));
            for (int j = 0; j < ruleRepresentation.size2(); j++) {
                ruleRepresentationTemp(i, j) = ruleRepresentation(i - (newRuleCount - 1), j);
            }
        }
    }
    // If there are no new rules, wtf
    else {
        spikingVectorTemp = spikingVector;
        delayVectorTemp = delayVector;
        ruleRepresentationTemp = ruleRepresentation;

        spikingVectorTemp(0, i_prime) = 0;
        delayVectorTemp(0, i_prime) = 0;

        for (int j = 0; j < ruleRepresentation.size2(); j++) {
            ruleRepresentationTemp(i_prime, j) = -1;
        }
    }

    // Shift all neuron related stuff
    for (int j = 0; j < j_prime; j++) {
        configVectorTemp (0, j) = configVector (0, j);
        configIDVectorTemp (0, j) = configIDVector (0, j);
    }
    for (int j = j_prime + 1; j < maxNeuronCount; j++) {
        configVectorTemp (0, j) = configVector (0, j - 1);
        configIDVectorTemp (0, j) = configIDVector (0, j - 1);
    }

    // Copy back the stuff
    spikingVector = spikingVectorTemp;
    delayVector = delayVectorTemp;
    ruleRepresentation = ruleRepresentationTemp;

    configVector = configVectorTemp;
    configIDVector = configIDVectorTemp;
    statusVector = statusVectorTemp;

    // Copy ID to ID vector
    configIDVector(0, j_prime) = child_1;
    configIDVector(0, j_prime + 1) = child_2;
    
    //cout << "Children: " << child_1 << " " << child_2 << endl;

    //cout << "RR0: " << ruleRepresentationComplete << endl;
    // Add the new rules
    int ii = i_prime;
    for (int i = 0; i < ruleRepresentation.size1(); i++) {
        if (ruleRepresentationComplete(i, 2) == child_1) {
            //cout << "i is" << i << endl;
            for (int j = 0; j < ruleRepresentation.size2(); j++) {
                ruleRepresentation(ii, j) = ruleRepresentationComplete(i, j);
            }
            ruleRepresentation(ii, 6) = j_prime;
            ii++;
        }
    }
    //cout << "RR1: " << ruleRepresentation << endl;
    for (int i = 0; i < ruleRepresentationComplete.size1(); i++) {
        if (ruleRepresentationComplete(i, 2) == child_2) {
            //cout << "i is " << i << endl;
            for (int j = 0; j < ruleRepresentation.size2(); j++) {
                ruleRepresentation(ii, j) = ruleRepresentationComplete(i, j);
            }
            ruleRepresentation(ii, 6) = j_prime + 1;
            ii++;
        }
    }

    // Adjust local_sigma for existing rules
    for (int i = i_prime + newRuleCount; i < ruleRepresentation.size1(); i++) {
        if (ruleRepresentation(i, 6) != -1) {
            ruleRepresentation(i, 6)++;
        }
    }

    
    for (int i = 0; i < ruleRepresentation.size1(); i++) {
        if (ruleRepresentation(i, 6) >= j_prime + 1) {
            if (ruleRepresentation(i, 6) == j_prime + 1 && ruleRepresentation(i, 2) == child_2) {
                continue;
            }
            //ruleRepresentation(i, 6)++;
        }
    }

    // update TM, RM and MV
    int i_actual = 0;

    transitionMatrix = reset(transitionMatrix);
    reductionMatrix = reset(reductionMatrix);

    for (int i = 0; i < maxRuleCount; i++) {
        int j = ruleRepresentation(i, 6);
        int j_nought = ruleRepresentation(i, 2);
      
        int rule_id = ruleRepresentation(i, 7);
        int rule_dic = -1;

        //cout << "j0 " << j_nought << endl;

        for (int h = 0; h < rules.size(); h++) {
            if (rules[h].sigma == j_nought &&
                rules[h].id == rule_id) {
                rule_dic = h;
                break;
            }
        }

        // If this is an empty rule
        if (j_nought == -1) {
            i_actual++;
            continue;
        }

        // Set up the mask vector
        if (rules[rule_dic].type == 2) {
            maskVector(0, i_actual) = 1;
        }
        else if (rules[rule_dic].type == 3) {
            maskVector(0, i_actual) = -1;
        }
        else {
            maskVector(0, i_actual) = 0;
        }

        // Update transition matrix, but only if the rule is a firing rule
        if (rules[rule_dic].type == 0) {
            // Check in the synapse dictionary for connected neurons
            for (int j_2 = 0; j_2 < maxNeuronCount; j_2++) {
                int j_check = configIDVector(0, j_2);

                // This is an empty neuron
                if (j_check == -1) {
                    continue;
                }
                // If they're the same neuron
                if (j_nought == j_check) {
                    continue;
                }
                if (synapses(j_nought, j_check) == 1) {
                    if (rules[rule_dic].sent < 0) {
                        //cout << "wtf" << rule_dic << endl;
                    }
                    transitionMatrix(i_actual, j_2) = rules[rule_dic].sent;
                }
            }
        }

        if (ruleRepresentation(i_actual, 2) != -1) {
            reductionMatrix(i_actual, j) = rules[rule_dic].consumed;
        }
        i_actual++;
    }

    //cout << endl;

    ii = 0;
    for (int i = 0; i < ruleRepresentation.size1(); i++) {
        for (int j = 0; j < rules.size(); j++) {
            if (ruleRepresentation(i, 7) == rules[j].id) {
                delayVector(0, ii) = rules[j].delay;
                break;
            }
        }
        ii++;
    }

    return newRuleCount;
}

matrix<int> compute_spiking_vector() {
    matrix <int> newSpikingVector (1, maxRuleCount);
    newSpikingVector = reset(newSpikingVector);
    // rulerep: {j, k, sigma, d', consumed, form, local sigma, id}
    
    for (int i = 0; i < ruleRepresentation.size1(); i++) {
        // int l = neuron that contains the ith rule mentioned above.
		int j = ruleRepresentation (i, 6);
        if (ruleRepresentation(i, 7) == -1) {
            continue;
        }

        if (statusVector(0, j) == 0) {
            newSpikingVector(0, i) = 0;
        }
        else {
            //cout << "i: " << i << endl;
			int sigma_spikes = configVector(0, j);
			bool does_spike = regexParser.solve(ruleRepresentation(i, 5), ruleRepresentation(i, 0), ruleRepresentation(i, 1), sigma_spikes);
            if (does_spike) {
                newSpikingVector(0, i) = 1;
            }
            else {
                newSpikingVector(0, i) = 0;
            }
        }
    }
	return newSpikingVector;
}

matrix<int> compute_spike_train() {
    matrix<int> newSpikeTrainVector (1, maxNeuronCount);
    newSpikeTrainVector = reset(newSpikeTrainVector);

    for (int j = 0; j < maxNeuronCount; j++) {
        int label = configIDVector(0, j);
        if (label == -1) {
            continue;
        }
        
        for (int i = 0; i < trains.size(); i++) {
            if (trains[i].label == label) {
                if (trains[i].delay <= timestep) {
                    if (trains[i].counter < trains[i].spike.size()) {
                        newSpikeTrainVector(0, j) = trains[i].spike[trains[i].counter++];
                    }
                }
            }
        }
    }

    return newSpikeTrainVector;
}

void read_input() {
	/*
	 * Order:
	 * Neurons (mode 0)
	 * Rules (mode 1)
	 * Synapses (mode 2)
	 * Spike trains (mode 3)
	 */
	using namespace boost;

	string string1;
	int mode = 0;
	int ctr = 0;

	getline(cin, string1);

    typedef tokenizer<char_separator<char> > tokenizer;
    char_separator<char> sep(" ");

    tokenizer init(string1, sep);
    tokenizer::iterator init_item = init.begin();
    init_item = init.begin();

	maxNeuronCount = stoi(*(init_item++));
	neuronCount = stoi(*(init_item++));
    maxRuleCount = stoi(*(init_item++));
	ruleCount = stoi(*(init_item++));
    iterations = stoi(*(init_item));

	synapses = matrix<int> (maxNeuronCount, maxNeuronCount);
	statusVector = matrix<int> (1, maxNeuronCount);

    neuron new_neuron(0, 0);

	while (getline(cin, string1)) {
		tokenizer toks(string1, sep);
        tokenizer::iterator item = toks.begin();
		item = toks.begin();

		if (string1[0] == '#') {
			// Comment line, skip this one
			continue;
		}
		else if (string1 == "NEXT") {
			mode++;
			ctr = 0;
			continue;
		}
		else if (string1 == "END") {
			break;
		}

		if (mode == 0) {
			/* label, spike */
			int label = stoi(*(item++));
            //string num = *item;
			int spike = stoi(*(item));

			new_neuron = neuron(label, spike);
			neurons.push_back(new_neuron);
		}
		else if (mode == 1) {
			/* j, k, consumed, type, sigma, form, id, sent/(child, child) */
			int j = stoi(*(item++));
			int k = stoi(*(item++));
			int c = stoi(*(item++));
			int type = stoi(*(item++));
			int sigma = stoi(*(item++));
			int form = stoi(*(item++));
            int id = stoi(*(item++));
			int spike = 0;
			int val_1 = 0;
			int val_2 = 0;

            //if (j < 0) cout << "b";

			if (type == 0 || type == 2) {
                val_1 = stoi(*(item++));
                val_2 = stoi(*(item));

				rule new_rule(j, k, c, type, sigma, form, id, val_1, val_2);
				rules.push_back(new_rule);
			}
			else if (type == 1 || type == 3) {
				rule new_rule(j, k, c, type, sigma, form, id);
				rules.push_back(new_rule);
			}
		}
		else if (mode == 2) {
			for (int i = 0; item != toks.end(); ++i, item++) {
				if (*item == "stop") {
					ctr++;
					break;
				}

				synapses(ctr, i) = stoi(*item);
			}
		}
		else if (mode == 3) {
            int delay;
            int label;
            int spike_mode;
            int spike_count;
            int spike;

            delay = stoi(*(item++));
            label = stoi(*(item++));
            spike_mode = stoi(*(item++));

            // same repeating one
            if (spike_mode == 0) {
                spike = stoi(*(item++));
                spike_train new_train(delay, label, spike_mode, spike);
                trains.push_back(new_train);
            }
            else if (spike_mode == 1) {
                spike_count = stoi(*(item++));
                spike_train new_train(delay, label, spike_mode);

                for (int i = 0; i < spike_count; i++) {
                    new_train.push(stoi(*(item++)));
                }
                trains.push_back(new_train);
            }
		}
	}

    cout << "Done reading input!" << endl;

	// Construct rule dictionary
    ruleCount = 0;
	for (int i= 0; i < maxNeuronCount; i++) {
		int id = neurons[i].label;
		neurons[i].index = i;
		for (int j = 0; j < ruleCount; j++) {
			if (id == rules[j].sigma) {
				rules[j].actual_index = i;
				ruleCount++;
			}
		}
	}

	// rule rep = {j, k, sigma, d', c, form, sigma_actual}

	ruleRepresentation = matrix<int> (maxRuleCount, 8);
	ruleRepresentationComplete = matrix<int> (maxRuleCount, 8);

	lossVector = matrix<int> (1, maxNeuronCount);
	gainVector = matrix<int> (1, maxNeuronCount);
	netGainVector = matrix<int> (1, maxNeuronCount);
	indicatorVector = matrix<int> (1, maxRuleCount);
	maskVector = matrix<int> (1, maxRuleCount);
	spikingVector = matrix<int> (1, maxRuleCount);
	delayVector = matrix<int> (1, maxRuleCount);
	transitionMatrix = matrix<int>(maxRuleCount, maxNeuronCount);
	reductionMatrix = matrix<int>(maxRuleCount, maxNeuronCount);
	configVector = matrix<int>(1, maxNeuronCount);
    divisionVector = matrix<int>(maxRuleCount, 2);
    configIDVector = matrix<int>(1, maxNeuronCount);
    spikeTrainVector = matrix<int>(1, maxNeuronCount);

    lossVector.clear();
    gainVector.clear();
    netGainVector.clear();
    indicatorVector.clear();
    maskVector.clear();
    spikingVector.clear();
    delayVector.clear();
    configVector.clear();
    transitionMatrix.clear();
    reductionMatrix.clear();
    spikeTrainVector.clear();

    //lossVector = reset(lossVector);
    //gainVector = reset(gainVector);
    //netGainVector = reset(netGainVector);
    //indicatorVector = reset(indicatorVector);
    //maskVector = reset(maskVector);
    //spikingVector = reset(spikingVector);
    //delayVector = reset(delayVector);
    //configVector = reset(configVector);
    //transitionMatrix = reset(transitionMatrix);
    //reductionMatrix = reset(reductionMatrix);
    matrix<int> buffer = reset_negative(ruleRepresentation);
    ruleRepresentation = buffer;
    matrix<int> buffer2 = reset_negative(ruleRepresentationComplete);
    ruleRepresentationComplete = buffer2;
    buffer = reset_negative(configIDVector);
    configIDVector = buffer;
    //spikeTrainVector = reset(spikeTrainVector);

    int ii = 0;
    int jj = 0;
    int rule_sentinel = maxRuleCount > rules.size() ? rules.size() : maxRuleCount; 
    int neuron_sentinel = maxNeuronCount > neurons.size() ? neurons.size() : maxNeuronCount;

    // Create transition matrix and reduction matrix
    for (int i = 0; i < rule_sentinel; i++) {
        int sigma = rules[i].sigma;
        if (neurons[sigma].spike == -1) {
            continue;
        }
        for (int j = 0; j < neuron_sentinel; j++) {
            if (neurons[j].spike == -1) {
                continue;
            }
            if (synapses(rules[i].sigma, j) == 1 &&
                j != rules[i].sigma &&
                rules[i].type == 0) {
                transitionMatrix(ii, jj) = rules[i].sent;
            }
            if (rules[i].sigma == jj) {
                reductionMatrix(ii, jj) = rules[i].consumed;
            }
            jj++;
        }

        if (rules[i].type == 2) {
            cout << rules[i].id << " " << rules[i].type << endl;
            maskVector(0, ii) = 1;
        }
        else if (rules[i].type == 3) {
            maskVector(0, ii) = -1;
        }
        delayVector(0, ii) = rules[i].delay;

        jj = 0;
        ii++;
    }
	
    // Create rule representations
    int rr_ctr = 0;
	for (int i = 0; i < rule_sentinel; i++) {
		ruleRepresentationComplete(i, 0) = rules[i].j;
		ruleRepresentationComplete(i, 1) = rules[i].k;
		ruleRepresentationComplete(i, 2) = rules[i].sigma;
		ruleRepresentationComplete(i, 3) = -1;
		ruleRepresentationComplete(i, 4) = rules[i].consumed;
		ruleRepresentationComplete(i, 5) = rules[i].form;
		ruleRepresentationComplete(i, 6) = rules[i].sigma;
        ruleRepresentationComplete(i, 7) = rules[i].id;

        int sigma = rules[i].sigma;
        if (neurons[sigma].spike != -1) {
            ruleRepresentation(rr_ctr, 0) = rules[i].j;
            ruleRepresentation(rr_ctr, 1) = rules[i].k;
            ruleRepresentation(rr_ctr, 2) = rules[i].sigma;
            ruleRepresentation(rr_ctr, 3) = -1;
            ruleRepresentation(rr_ctr, 4) = rules[i].consumed;
            ruleRepresentation(rr_ctr, 5) = rules[i].form;
            ruleRepresentation(rr_ctr, 6) = rules[i].sigma;
            ruleRepresentation(rr_ctr, 7) = rules[i].id;
            rr_ctr++;
        }
	}

    // Create config vector
	int real_i = 0;
	for (int i = 0; i < neuron_sentinel; i++) {
		if (neurons[i].spike != -1) {
			configVector(0, real_i) = neurons[i].spike;
            configIDVector(0, real_i) = neurons[i].label;
		}

        real_i++;
	}
    
    real_i = 0;
	for (int i = 0; i < maxNeuronCount; i++) {
        for (int j = 0; j < maxRuleCount; j++) {
            if (ruleRepresentation(j, 2) == i) {
                ruleRepresentation(j, 6) = real_i;
            }
        }
        real_i++;
	}

	// Set status vector
	for (int i = 0; i < statusVector.size2(); i++) {
		statusVector(0, i) = 1;
	}

    cout << "SV: " << spikingVector << endl;
	cout << "StV: " << statusVector  << endl;
	cout << "RR: " << ruleRepresentation << endl;
    cout << "DV: " << delayVector << endl;
	cout << "CV: " << configVector << endl;
    cout << "ID: " << configIDVector << endl << endl;
}

void display_rr() {
    cout << "RR: ";
    for (int i = 0; i < maxRuleCount; i++) {
        cout << ruleRepresentation(i, 2) << " ";
    }
    cout << endl;
}

void display_cfg() {
    cout << "CLV:CV: { ";
    for (int i = 0; i < maxNeuronCount; i++) {
        cout << "(" << configIDVector(0, i) << ":" << configVector(0, i) << "), ";
    }
    cout << "}" << endl;
}
