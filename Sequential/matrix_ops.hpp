#include <iostream>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <utility>

using namespace std;
using namespace boost::numeric::ublas;

matrix<int> multiply_vectors(matrix<int>, matrix<int>);
matrix<int> reset(matrix<int>);
matrix<int> reset_opposite(matrix<int>);
matrix<int> reset_negative(matrix<int>);
matrix<int> matrix_elementwise_mult(matrix<int>, matrix<int>);
