#include "train.hpp"

spike_train::spike_train(int _delay, int _label, int _spike_type, int _spike) {
    delay = _delay;
    label = _label;
    spike_mode = _spike_type;
    spike.push_back(_spike);
    counter = 0;
}

spike_train::spike_train(int _delay, int _label, int _spike_type) {
    delay = _delay;
    label = _label;
    spike_mode = _spike_type;
    counter = 0;
}

void spike_train::push(int _spike){
    spike.push_back(_spike);
}
