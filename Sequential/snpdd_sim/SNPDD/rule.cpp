#include "rule.hpp"

rule::rule (int _j, int _k, int _consumed, int _type, int _sigma, int _form, int _id) {
    sent = -1;
    child1 = -1;
    delay = 0;
    child2 = -1;
	j = _j;
	k = _k;
	consumed = _consumed;
	type = _type;
	sigma = _sigma;
	form = _form;
    id = _id;
}

rule::rule (int _j, int _k, int _consumed, int _type, int _sigma, int _form, int _id, int _val1, int _val2) {
	j = _j;
	k = _k;
	consumed = _consumed;
	type = _type;
	sigma = _sigma;
	form = _form;
    id = _id;

    delay = 0;
    sent = -1;
    child1 = -1;
    child2 = -1;

    if (type == 0) {
        sent = _val1;
        delay = _val2;
    }
    else if (type == 2) {
        child1 = _val1;
        child2 = _val2;
    }
}

void rule::display_children() {
    std::cout << "[" << child1 << ", " << child2 << "]" << std::endl;
}
