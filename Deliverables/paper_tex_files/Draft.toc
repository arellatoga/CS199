\select@language {english}
\contentsline {section}{\numberline {1}Introduction}{1}
\contentsline {section}{\numberline {2}Theoretical Framework}{1}
\contentsline {section}{\numberline {3}Related Work}{2}
\contentsline {subsection}{\numberline {3.1}Spiking Neural P Systems with Division and Dissolution}{2}
\contentsline {subsection}{\numberline {3.2}Definitions}{5}
\contentsline {subsection}{\numberline {3.3}Algorithm for Spiking Neural P Systems}{7}
\contentsline {subsection}{\numberline {3.4}Regular Expressions}{8}
\contentsline {subsection}{\numberline {3.5}Software aspects}{9}
\contentsline {section}{\numberline {4}Algorithm for Spiking Neural P Systems with Division and Dissolution}{9}
\contentsline {subsection}{\numberline {4.1}Redefinitions}{9}
\contentsline {subsection}{\numberline {4.2}Working Algorithm}{11}
\contentsline {section}{\numberline {5}Simulation of the \textit {SAT} Problem}{15}
\contentsline {section}{\numberline {6}Software Implementation}{22}
\contentsline {subsection}{\numberline {6.1}Technical specifications}{22}
\contentsline {subsection}{\numberline {6.2}Experimental Results}{23}
\contentsline {section}{\numberline {7}Conclusion}{26}
