#include <iostream>
#include <vector>

class spike_train {
    public:
    spike_train(int, int, int, int);
    spike_train(int, int, int);
    void push(int);
    int delay;
    int label;
    int spike_mode;
    std::vector<int> spike;
    int counter;
};
