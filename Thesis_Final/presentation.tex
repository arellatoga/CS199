\documentclass{beamer}
%\usepackage{times}
\usepackage{amsmath}
\usepackage{tikz}
\usepackage{amsfonts}
\usepackage[english]{babel}

\usepackage[utf8]{inputenc}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{float}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{pstricks}
\setlength\parindent{0pt}

\usetikzlibrary{calc}

\newtheoremstyle{break}
{\topsep}{\topsep}
{}{}
{\bf}{:}
{\newline}{}
\theoremstyle{break}

\newtheorem{defn}{Definition}[section]
\newtheorem{eqtn}{Equation}[section]

\title{Creation of a Simulator for Spiking Neural P Systems
    with Neuron Division and Dissolution}
\author{Gerard Arel H. Latoga}

\begin {document}

\frame{\titlepage}

\begin{frame}
\frametitle{Abstract}
    Spiking Neural P (SNP) systems are a type of spiking neural network model composed of neurons sending
    spikes to other neurons through synapses. A modification of the SNP system, the Spiking
    Neural P System with Division and Dissolution,
    has been designed to allow for neurons to not only spike and forget, but also divide into 
    two other defined neurons and dissolve itself from the system. Simulation of such systems by hand would be 
    inefficient, as at higher neuron count, keeping progress would prove to be cumbersome. As such, a simulator
    for SNP systems with Division and Dissolution was patterned after an earlier program that simulates
    simpler SNP systems.
\end{frame}

%%%%%

\section{Theoretical Framework}

\begin{frame}
\frametitle{Theoretical Framework}
\begin{itemize}
    \item
        SNPs are built upon \textbf{regular languages}.
    \item
        SNPs have functions similar to finite automata.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Theoretical Framework}
\begin{alertblock}{Definition: Regular Languages}
    A language is called a \textit{regular language} if some 
    finite automaton recognizes it.
\end{alertblock}
\end{frame}

\begin{frame}[shrink=5]
\frametitle{Theoretical Framework}
\centering
\begin{alertblock}{Definition: Spiking Neural P Systems} 
    An SNP system of a finite degree $m \leq 1$, according to \cite{snp-tutorial},
    is a construct o the form \\
    $\Pi = (O, \sigma_1,...,\sigma_m, syn, in, out)$, where :
    \begin{enumerate}
        \item
            $ O = \{a\}$ is the singleton alphabet of spike $a$.
        \item
            $\sigma_1,...,\sigma_m$ are \textit{neurons}, taking the form of
            $\sigma_i =(n_i, R_i), 1 \leq i \leq m$, where:
            \begin{enumerate}
                \item 
                    $n_i \geq 0$ is the initial number of spikes contained in $\sigma_i$;
                \item
                    $R_i$ is a finite set of rules of the following two forms:
                    \begin{itemize}
                        \item
                            $E/a^c \rightarrow a^P;d$, where $E$ is a regular expression over
                            $a$ and $c \geq p \geq 1, d \geq 0$
                        \item 
                            $a^s \rightarrow \lambda$, for $s \geq 1$, with the restriction that 
                            for each rule $E/a^c \rightarrow a^P;d$ of type (1) from $R_i$ we have
                            $a^s \not\in L(E)$
                    \end{itemize}
            \end{enumerate}
        \item
            $syn \subseteq \{1,2,...,m\}\times\{1,2,...,m\}$ with $i /neq j$ for all $(i,j) \in syn, 1 \leq i, j \leq m$ (synapses between neurons)
        \item
            $in, out \in \{1, 2, ..., m\}$ indicate the input and the output neurons, respectively
    \end{enumerate}
\end{alertblock}
\end{frame}

\begin{frame}
\frametitle{}
    \begin{defn}[\textbf {Configuration Vectors}]
    A \textit{configuration vector} $C_k = \langle \alpha_1,...,\alpha_2 \rangle,\alpha_j \in \mathbb{N}$
    is a vector representing the number of spikes present in each neuron pertaining to the $kth$ time step.
    \end{defn}
\end{frame}

\begin{frame}
\frametitle{}
    \begin{defn}[\textbf {Spiking Vectors}]
    A \textit{spiking vector} $S_k = <S_{k_(1)},...,S_{k_(n)} >$ corresponds to a $C_k$ such that
    \begin{enumerate}
        \item
            for each $i$ $(1\leq i \leq n), S_{k_(i)} = 1$ whenever rule $r_i$ is applicable to 
            $C_k$, otherwise $S_{k_(i)} = 0$
        \item
            if $\{i_1,...,i_t\}$ verifies $S_{k_(i)} = 1 (1 \leq j \leq t)$ and $S_{k_(i)} = 0$
            for $p \not\in \{i_1,...,i_t\}$, then rules $r_{i1},...,r_{it}$ are simultaneously applicable
            to $C_k$
    \end{enumerate}
    \end{defn}
\end{frame}

\begin{frame}
\frametitle{}
    \begin{defn}[\textbf{Spiking Transition Matrix}]
    The \textit{spiking transition matrix} $M_\Pi$ is an $n \times m$ matrix consisting of elements
    $a_{ij}$ given as
    \begin{equation}
        a_{ij} =
        \begin{cases}
            -c & \text{rule $r_i$ is in $\sigma_j$ and consumes $c$ spikes}\\
            p & \text{rule $r_i$ is in $\sigma_s ((s,j)\in syn)$ producing $p$ spikes in total}\\
            0 & \text{rule $r_i$ is in $\sigma_s ((s,j)\in syn)$}
        \end{cases}
    \end{equation}
    \end{defn}
\end{frame}


\begin{frame}
\frametitle{}
    \begin{defn}[\textbf{Status Vector}]
    The vector $St_k = \langle st_1,...,st_m \rangle$ is the status vector at the $kth$ step for each neuron represented
    by $st_i$ where
    \begin{equation}
        st_i =
        \begin{cases}
            1 & \text{if neuron $m$ is open}\\
            0 & \text{if neuron $m$ is closed}
        \end{cases}
    \end{equation}
    \end{defn}
    %
\end{frame}


\begin{frame}
\frametitle{}
\begin{defn}[\textbf{Rule Representation}]
    The set $R^k = r_1, r_2, ..., r_n$ is the set of rules of count $n$ present in $\Pi$.
    Each $r_i$ is defined to be $r_i = \{E,j,d',c \}$ where the regular expression for rule $i$ is $E$,
    and rule $i$ is contained in neuron $j$,
    \begin{equation}
        d_i =
        \begin{cases}
            -1 & \text{if the rule is inactive}\\
            0 & \text{if the rule is fired}\\
            \leq 1 & \text{if the rule is currently on delay}
        \end{cases}
    \end{equation}
    and $c$ is the number of spikes $\sigma_j$ will consume if rule $i$ is applied.
    \end{defn}
\end{frame}


\begin{frame}
\frametitle{}
    \begin{defn}[\textbf {Delay Vector}]
    The delay vector $D = \langle d_1,...,d_m \rangle$ stores the amount of delay for each rule $r_i, i = 1,...,n$ in $\Pi$ 
    \end{defn}
\end{frame}


\begin{frame}
\frametitle{}
\begin{defn}[\textbf{RM}]
    For a system composed of $m$ neurons and $n$ rules, the reduction matrix $RM = \langle rm_1, rm_2,..., rm_n \rangle$
    indicates the spikes consumed by a neuron per rule where
    \begin{equation}
        rm_i =
        \begin{cases}
            c & \text{if rule $r_i$ is contained in neuron $\sigma_j$ and consumes $c$ spikes to apply}\\
            0 & \text{if rule $r_i$ is not contained in neuron $\sigma_j$}
        \end{cases}
    \end{equation}
    \end{defn}

\end{frame}

\begin{frame}
\frametitle{}
\begin{defn}[\textbf{Loss Vector}]
    The loss vector $LV_k = \langle lv_1,...,lv_m \rangle$ is the vector where for $lv_i$ for each neuron $\sigma_i, i =1,...,m$
    contains the number of spikes consumed of the rule in the specific neuron $\sigma_i$ at the $kth$ step.
    The loss vector can be obtained by multipling the spiking vector with $RM$.
    \end{defn}

\end{frame}

\begin{frame}
\frametitle{}
\begin{defn}[\textbf{Gain Vector}]
    The gain vector $GV_k = \langle gv_1,...,gv_m \rangle$ is the gain vector which contains the total number of spikes gained,
    $gv_i$ for each neuron $\sigma_i, i =1,...,m$ at the $kth$ step, regardless if it is open or closed. 
    \end{defn}

\end{frame}

\begin{frame}
\frametitle{}
\begin{defn}[\textbf{Transition Vectors}]
    Let $d:1,...,n$ be a total order given for all the $n$ rules, the transition vectors of $\Pi$,
    is a set of vectors $Tv$ defined as $Tv = \{tv_1,....,tv_n\}$ where each $tv_i=<p_1,...,p_m>$ and
    \end{defn}
    \begin{equation}
        p_j =
        \begin{cases}
            p & \text{if rule $r_i$ is in $\sigma_s (s \neq j$ and $ (s \neq j) \in syn)$ and it is applied producing $p$ spikes} \\
            0 & \text{if rule $r_i$ is in neuron $\sigma_s (s \neq j$ and $ (s \neq j) \in syn)$}
        \end{cases}
    \end{equation}

\end{frame}

\begin{frame}
    \frametitle{}
\begin{defn}[\textbf{Indicator Vector}]
    The indicator vector $IV_k = \langle iv_1,...,iv_m \rangle$ will be mupltiplied to the transition vector
    to obtain the net number of spikes a neuron receives, regardless if it's open or closed.
    \end{defn}
\end{frame}

\begin{frame}
    \frametitle{}\begin{defn}[\textbf{Net Gain Vector}]
    The net gain vector $NG_k = GV_k \otimes st_k - LV_k$ at step $k$ where $\otimes$ is the element-wise
    multiplication operator.
    Finally, we have the equation to compute for the configuration vectors themselves.
    \end{defn}

\end{frame}

\begin {frame}
    \frametitle{Algorithm}
    \begin{center}
    \scalebox{0.5}{
        \begin{minipage}{0.7\linewidth}
        \begin{algorithm}[H]
            \caption{Algorithm for SNP systems}
            \begin{algorithmic}[1]
                \Procedure{Simulate SNPDD}{}\Comment{Modified algorithm for SNPDDs}
                \State $Reset(LV^k)$
                \State $Reset(GV^k)$
                \State $Reset(NG^k)$
                \State $Reset(IV^k)$
                \State $Compute (S^k)$
                \For{$r_i = \{E, j, d', c\} \in R$}
                    \If {$S^k_i = 1$}
                        \State $LV^k_j \leftarrow c$
                        \State $d' \leftarrow d_i$
                        \State $IV^k_i \leftarrow 0$
                        \If{$d' = 0$}
                            \State $IV^k_i \leftarrow 1$
                            \State $St^k_i \leftarrow 1$
                        \EndIf
                    \ElsIf{$d' = 0$}
                        \State $IV^k_i \leftarrow 1$
                        \State $St^k_i \leftarrow 1$
                    \EndIf
                \EndFor
                \State $GV^k \leftarrow TV * IV^k$
                \State $NG^k \leftarrow GV^k \otimes St^k - LV^k$
                \State $C^{k+1} \leftarrow C^k + NG^k$
                \For{$r_i = \{E, j, d', c\} \in R$}
                    \If {$d' \neq -1$}
                        \State $d' \leftarrow d' - 1$
                    \EndIf
                \EndFor
                \State \Return $C^{k+1}$
                \EndProcedure
            \end{algorithmic}
        \end{algorithm}
    \end{minipage}
    }
\end{center}
\end{frame}

\begin{frame}[shrink=10]
\frametitle{Related Work}
\begin{alertblock}{Definition: Spiking Neural P Systems with Division and Dissolution} 
An SNPDD system of a finite degree $m \leq 1$ is a construct of the form\\
    $\Pi = (O, H, syn, n_1, n_2,...,n_m,R,in,out)$, where:
    \begin{enumerate}
        \item
            $O = \{s\}$ is the \textit{singleton alphabet} where $s$ is a spike.
        \item
            $H$ is the set of \textit{labels} for the neurons.
        \item
            $syn \subseteq H \times H$ represents a \textit{dictionary of the synapses} for each
            and every neuron ($1 \leq i \leq m, (i,i) \notin syn$)
        \item
            $n_i \geq 0$ represents the \textit{initial number of spikes} in neuron $\sigma_i$ for
            $1 \leq i \leq m$.
        \item
            $R$ represents the set of \textit{all developmental rules}, taking either of the four forms:
            \begin{itemize}
                \item Firing rule $[E/a^c\rightarrow a^P;d]$
                \item Forgetting rule $[E/a^c \rightarrow \lambda]$
                \item Neuron Division rule $[E]_i \rightarrow []_j || []_k$
                \item Neuron Dissolution rule $[E]_i \rightarrow \sigma_i$
            \end{itemize}
        \item
            $in, out \subseteq H$ indicate the \textit{input and output} neurons of $\Pi$, respectively.
    \end{enumerate}

\end{alertblock}
\end{frame}

\section{Statement of the Problem}
\begin{frame}
\frametitle{Statement of the Problem}
\begin{enumerate}
    \item<1->
        Simulation via hand can get messy.
    \item<2->
        SNPDDs can be represented into vectors and matrices.
\end{enumerate}
\end{frame}

\section{Significance of the Study}
\begin{frame}
\frametitle{Significance of the Study}
    \begin{enumerate}
        \item<1->
            SNPDDs are capable of solving NP-hard problems in linear time.
        \item<2->
            A simulator can significantly lessen time spent on computing.
        \item<3->
            Provide a basis for future simulators.
    \end{enumerate}
\end{frame}

\section{Objectives of the Study}
\begin{frame}
    \frametitle{Objectives of the Study}
    \begin{enumerate}
        \item<1->
            Represent the SNPDD system into vectors and matrices.
        \item<2->
            Formulate an algorithm that allows computation of SNPDD systems,
        \item<2->
            Create a proof-of-concept simulator
    \end{enumerate}
\end{frame}

\section{Scope and Limitations of the Study}
\begin{frame}
    \frametitle{Scope and Limitations of the Study}
    \begin{enumerate}
        \item<1->
            The algorithm must work on any SNPDD as input.
        \item<2->
            Must be backwards compatible.
        \item<3->
            Not all SNPDD systems will work due to time and space constraints
    \end{enumerate}
\end{frame}

\section{Research Plan}
\begin{frame}
    \frametitle{Research Plan}
    \begin{enumerate}
        \item
            Further review of literature related to the topic at hand.
        \item
            Study the nature of SNPDD systems and possible patterns that may arise.
        \item
            Formulate an algorithm for computing SNPDD systems
        \item
            Programming of simulator
    \end{enumerate}
\end{frame}

\section{Improvised Algorithm}

\begin{frame}
\frametitle{Definition}
    \begin{defn}[\textbf{Mask Vector}]
        Given $n$ rules present on the system at the $kth$ step,
        a \textbf{Mask Vector} $MV^{(k)}$ is defined as 
        $MV^{(k)} = \{ mv_1, mv_2, ..., mv_n \}$ 
        where 
        \begin{equation}
        mv^{k}_j =     
        \begin{cases}
            -1 & \text{if the rule is a dissolution rule} \\
            0 & \text{if the rule is either a spiking or forgetting rule} \\ 
            1 & \text{if the rule is a division rule}
        \end{cases}
        \end{equation} 
    \end{defn}
\end{frame}

\begin{frame}
\frametitle{Definition}
\begin{defn}[\textbf{Spike Train Vector}]
     A spike train can be defined as the construct:
        $$ST_i = (E, \sigma_i, a)$$
        \begin{enumerate}
            \item $E$ is the regular expression of the spike train
            \item $\sigma$ is the neuron it is attached to, identified by its index $i$ and
            \item $a$ is the number of spikes it sends
        \end{enumerate}
        The \textbf{Spike Train Vector} $STV^{(k)}$ is a vector which size is the same as the
        number of neurons present in the system $m$ and is defined as
        $STV^{(k)} = \langle stv_1, stv_2, ..., stv_m \rangle$ where 
        \[
        stv_i =
        \begin{cases}
            a_i & \text{if $E_i$ of $ST_i$ is satisfied by the current $k$} \\
            0 & \text{otherwise}
        \end{cases}
        \] 
    \end{defn}
\end{frame}

\begin{frame}
\frametitle{Redefinition}
    \begin{defn}[\textbf{Rule Representation}]
    % rulerep: {j, k, sigma, d', consumed, form, local sigma, id}
    The set $R^k = r_1, r_2,...r_n$ has been redefined int othe following construct:
    $$ r_i = \{E, i_\circ, j, j_\circ, d'\}$$ 
    where $E$, is the regular expression use in rule $i, j$ ,$i_\circ$
    is the rule's index for the rule dictionary $R$, $j_\circ$ is $j$'s label in $H$,
    and $d'$ is the rule's fire status.
    \end{defn}
\end{frame}

\begin {frame}
    \frametitle{Algorithm}
    \begin{center}
    \scalebox{0.33}{
        \begin{minipage}{0.7\linewidth}
        \begin{algorithm}[H]
            \caption{Modified Algorithm for SNPDDs}
            \begin{algorithmic}[1]
                \Procedure{Simulate SNPDD}{}\Comment{Modified algorithm for SNPDDs}
                \State $Reset(LV^k)$
                \State $Reset(GV^k)$
                \State $Reset(NG^k)$
                \State $Reset(IV^k)$
                \State $Compute (S^k)$
                \State $Acquire(STV^k)$ \Comment{Check if spike trains are applicable}
                \State $CV \leftarrow S^k \otimes MV$ \Comment{Checker Vector, Elementwise multiplication of $S^k$ and $MV$}
                \State $offset \leftarrow 0$
                \For{$cv \in CV$}
                    \If {$cv_j = 1$} \Comment {Check if the rule is a division rule}
                        \State $S^k_i \leftarrow 0$ \Comment{Then set the spiking vector value to 0}
                        \State $DivideNeuron(j + offset)$
                        \State $offset \leftarrow offset + 1$
                    \ElsIf {$cv_j = 0$} \Comment {Check if the rule is a dissolution rule}
                        \State $DissolveNeuron(j)$
                    \EndIf
                \EndFor
                \For{$r_i = \{ E, i_\circ, j, j_\circ, d' \} \in R$} \Comment{Perform the rest of the algorithm}
                    \If {$S^k_i = 1$}
                        \State $LV^k_j \leftarrow c$
                        \State $d' \leftarrow d_i$
                        \State $IV^k_i \leftarrow 0$
                        \If{$d' = 0$}
                            \State $IV^k_i \leftarrow 1$
                            \State $St^k_i \leftarrow 1$
                        \EndIf
                    \ElsIf{$d' = 0$}
                        \State $IV^k_i \leftarrow 1$
                        \State $St^k_i \leftarrow 1$
                    \EndIf
                \EndFor
                \State $GV^k \leftarrow STV^k + (TV * IV^k)$
                \State $NG^k \leftarrow GV^k \otimes St^k - LV^k$ \Comment{Modified to also add the $STV$}
                \State $C^{k+1} \leftarrow C^k + NG^k$
                \For{$r_i = \{E, j, d', c\} \in R$}
                    \If {$d' \neq -1$}
                        \State $d' \leftarrow d' - 1$
                    \EndIf
                \EndFor
                \State \Return $C^{k+1}$
                \EndProcedure
            \end{algorithmic}
        \end{algorithm}


    \end{minipage}
    }
\end{center}
\end{frame}

\section{Software Implementation}
\subsection {Technical implementation}
\begin{frame}
    \frametitle{Limitations}
    The maximum value of neurons and rules must be computed by the user.
    These values will be used as widths and heights for the vectors and matrices.
\end{frame}

\subsection {Software specifications}
\begin{frame}[shrink=20]
    \frametitle{Input handling}
    \texttt{j\_max, j\_init, i\_max, i\_init, iter}\\
    \texttt{label, spike}\\
    ...\\
    \texttt{label, spike}\\
    \texttt{NEXT}\\
    \texttt{j, k, c, type, sigma, form, id, val\_1, val\_2}\\
    ...\\
    \texttt{j, k, c, type, sigma, form, id, val\_1, val\_2}\\
    \texttt{NEXT}\\
    \texttt{0 0 0 0 0 0 0 stop}\\
    ...\\
    \texttt{0 0 0 0 0 0 0 stop}\\
    \texttt{NEXT}\\
    \texttt{j, k, form, label, type, n\_1, n\_2, ...n\_a}\\
    ...\\
    \texttt{j, k, form, label, type, n\_1, n\_2, ...n\_a}\\
\end{frame}

\section{Closing Remarks}
\begin{frame}
    \frametitle{Thank yous}
    Thanks to Sir Cabarle for being a cool adviser.\\
    Thanks to all ACL lab members for being in the fight. \\
    Thanks to all my friends for inspiration. \\
    Thanks to the various coffee manufacturers. \\
    Thanks to my cat, Owange, for being with me during sleepless nights.
\end{frame}

\section{Bibliography}
\frametitle{Bibliography}
\begin{frame}[shrink=50]
\nocite{*}
\bibliography{refs}
\bibliographystyle{apalike}
\end{frame}

\end{document}
