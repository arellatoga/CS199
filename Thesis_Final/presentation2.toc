\select@language {english}
\beamer@sectionintoc {1}{Introduction}{3}{0}{1}
\beamer@sectionintoc {2}{Definitions}{4}{0}{2}
\beamer@subsectionintoc {2}{1}{Spiking Neural P Systems}{4}{0}{2}
\beamer@subsectionintoc {2}{2}{Spiking Neural P Systems with Divison and Dissolution}{5}{0}{2}
\beamer@subsectionintoc {2}{3}{Vectors and Matrices}{26}{0}{2}
\beamer@sectionintoc {3}{Algorithm Outline}{42}{0}{3}
\beamer@subsectionintoc {3}{1}{Algorithm for solving SNPDD}{42}{0}{3}
\beamer@subsectionintoc {3}{2}{Algorithm for acquiring the Spike Train Vector}{43}{0}{3}
\beamer@subsectionintoc {3}{3}{Algorithms for Division and Dissolution}{44}{0}{3}
\beamer@sectionintoc {4}{Experimentation}{46}{0}{4}
\beamer@subsectionintoc {4}{1}{Limitations of the Software}{46}{0}{4}
\beamer@subsectionintoc {4}{2}{Results}{47}{0}{4}
\beamer@subsectionintoc {4}{3}{Results}{47}{0}{4}
\beamer@sectionintoc {5}{Conclusion}{57}{0}{5}
\beamer@subsectionintoc {5}{1}{Conclusion}{57}{0}{5}
\beamer@subsectionintoc {5}{2}{Prospects}{58}{0}{5}
\beamer@subsectionintoc {5}{3}{Thanks}{59}{0}{5}
\beamer@sectionintoc {6}{Bibliography}{60}{0}{6}
