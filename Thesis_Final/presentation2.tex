\documentclass{beamer}
\usepackage{amsmath}
\usepackage{tikz}
\usepackage{amsfonts}
\usepackage[english]{babel}

\usepackage[utf8]{inputenc}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{float}
\usepackage{amsthm}
\usepackage{mathtools}
%\usepackage{pstricks}
\usepackage{xcolor}
\definecolor{darkblue}{rgb}{0, 0, 0.5}
\usepackage{transparent}
\usepackage[adobe-utopia]{mathdesign}
\usepackage[pdf]{pstricks}
\setlength\parindent{0pt}

\usetikzlibrary{calc}

\newtheoremstyle{break}
{\topsep}{\topsep}
{}{}
{\bf}{:}
{\newline}{}
\theoremstyle{break}

\newtheorem{defn}{Definition}[section]
\newtheorem{eqtn}{Equation}[section]

\title{Creation of a Simulator for Spiking Neural P Systems
    with Neuron Division and Dissolution}
\author{Gerard Arel H. Latoga}

\begin {document}

\frame{\titlepage}

\frame{\tableofcontents}

\section{Introduction}
\begin{frame}
\frametitle{Abstract}
    Spiking Neural P (SNP) systems are a type of spiking neural network model composed of neurons sending
    spikes to other neurons through synapses. A modification of the SNP system, the Spiking
    Neural P System with Division and Dissolution,
    has been designed to allow for neurons to not only spike and forget, but also divide into 
    two other defined neurons and dissolve itself from the system. Simulation of such systems by hand would be 
    inefficient, as at higher neuron count, keeping progress would prove to be cumbersome. As such, a simulator
    for SNP systems with Division and Dissolution was patterned after an earlier program that simulates
    simpler SNP systems.
\end{frame}

\section{Definitions}
    \subsection{Spiking Neural P Systems}
    \begin{frame}[shrink=10]
            \frametitle{Spiking Neural P Systems}
        \centering
        \begin{alertblock}{Definition: Spiking Neural P Systems} An SNP system of a finite degree $m \leq 1$, according to \cite{snp-tutorial}, is a construct o the form \\ $\Pi = (O, \sigma_1,...,\sigma_m, syn, in, out)$, where : \begin{enumerate} \item $ O = \{a\}$ is the singleton alphabet of spike $a$.
                \item
                    $\sigma_1,...,\sigma_m$ are \textit{neurons}, taking the form of
                    $\sigma_i =(n_i, R_i), 1 \leq i \leq m$, where:
                    \begin{enumerate}
                        \item 
                            $n_i \geq 0$ is the initial number of spikes contained in $\sigma_i$;
                        \item
                            $R_i$ is a finite set of rules of the following two forms:
                            \begin{itemize}
                                \item
                                    $E/a^c \rightarrow a^P;d$, where $E$ is a regular expression over
                                    $a$ and $c \geq p \geq 1, d \geq 0$
                                \item 
                                    $a^s \rightarrow \lambda$, for $s \geq 1$, with the restriction that 
                                    for each rule $E/a^c \rightarrow a^P;d$ of type (1) from $R_i$ we have
                                    $a^s \not\in L(E)$
                            \end{itemize}
                    \end{enumerate}
                \item
                    $syn \subseteq \{1,2,...,m\}\times\{1,2,...,m\}$ with $i /neq j$ for all $(i,j) \in syn, 1 \leq i, j \leq m$ (synapses between neurons)
                \item
                    $in, out \in \{1, 2, ..., m\}$ indicate the input and the output neurons, respectively
            \end{enumerate}
        \end{alertblock}
        \end{frame}

    \subsection{Spiking Neural P Systems with Divison and Dissolution}
    \begin{frame}[shrink=10]
        \frametitle{Spiking Neural P Systems with Division and Dissolution}
        \begin{alertblock}{Definition: SNPDDs}
            An SNPDD system of a finite degree $m \leq 1$ is a construct of the form\\
            $\Pi = (O, H, syn, n_1, n_2,...,n_m,R,in,out)$, where:
            \begin{enumerate}
                \item
                    $O = \{a\}$ is the \textit{singleton alphabet} where $s$ is a spike.
                \item
                    $H$ is the set of \textit{labels} for the neurons.
                \item
                    $syn \subseteq H \times H$ represents a \textit{dictionary of the synapses} for each
                    and every neuron ($1 \leq i \leq m, (i,i) \notin syn$)
                \item
                    $n_i \geq 0$ represents the \textit{initial number of spikes} in neuron $\sigma_i$ for
                    $1 \leq i \leq m$.
                \item
                    $R$ represents the set of \textit{all developmental rules}, taking either of the four forms:
                    \begin{itemize}
                        \item Firing rule $[E/a^c\rightarrow a^P;d]$
                        \item Forgetting rule $[E/a^c \rightarrow \lambda]$
                        \item Neuron Division rule $[E]_i \rightarrow []_j || []_k$
                        \item Neuron Dissolution rule $[E]_i \rightarrow \delta$
                    \end{itemize}
                \item
                    $in, out \subseteq H$ indicate the \textit{input and output} neurons of $\Pi$, respectively.
            \end{enumerate}
        \end{alertblock}
    \end{frame}

    \begin{frame}[shrink=0]
        \frametitle{Sample SNPDD}
        \begin{figure}[H]
            \centering
            \includegraphics{sample0}
            \caption{Sample SNPDD at step $k = 0$}
            \label{fig:sample0}
        \end{figure}
    \end{frame}

    \begin{frame}
        \begin{itemize}
            \item $O = \{a\}$
            \item $H = \{ 0, 1, 2, 3, 4 \}$
            \item $syn = \{ (1, 0) , (1, 2), (2, 1), (1,3) , (1, 4)    \}$
            \item $ n_1 = 0, n_2 = 1, n_3 = 2 $
            \item $R = $
            \begin{itemize}
                \item $ [a]_0 \rightarrow [\ ]_3\  ||\ [\ ]_4$
                \item $ [a \rightarrow a]_1$
                \item $ [a \rightarrow a]_2$
                \item $ [a]_4 \rightarrow \delta $
            \end{itemize}
            \item $in = 1$, $out = 3, 4$
        \end{itemize}
    \end{frame}

    \begin{frame}
        \frametitle{Input and output}
        \begin{itemize}
            \item Input is received by the \textit{input} neurons via \textit{spike trains}.
            \item Output is represented by designated neurons specially labelled to display information
        \end{itemize}
    \end{frame}

    \begin{frame}
        \frametitle{Spike Trains}
        \begin{itemize}
            \item Of the form $a_1, a_2, ... a_t $  for $t$ spikes to be sent 
                to a neuron with a label $h$
            \item Several spike trains for different input neurons
        \end{itemize}
    \end{frame}

    \begin{frame}[shrink=0]
        \frametitle{Sample SNPDD}
        \begin{figure}[H]
            \centering
            \includegraphics[scale=0.5]{sample1}
            \caption{Sample SNPDD at step $k = 1$}
            \label{fig:sample1}
        \end{figure}
    \end{frame}

    \begin{frame}[shrink=0]
        \frametitle{Sample SNPDD}
        \begin{figure}[H]
            \centering
            \includegraphics[scale=0.5]{sample2}
            \caption{Sample SNPDD at step $k = 2$}
            \label{fig:sample2}
        \end{figure}
    \end{frame}

    \begin{frame}[shrink=0]
        \frametitle{Sample SNPDD}
        \begin{figure}[H]
            \centering
            \includegraphics[scale=0.5]{sample3}
            \caption{Sample SNPDD at step $k = 3$}
            \label{fig:sample3}
        \end{figure}
    \end{frame}

    \begin{frame}[shrink=0]
        \frametitle{Sample SNPDD}
        \begin{figure}[H]
            \centering
            \includegraphics[scale=0.5]{sample4c}
            \caption{Sample SNPDD at step $k = 4$}
            \label{fig:sample4}
        \end{figure}
    \end{frame}

    \begin{frame}[shrink=0]
        \frametitle{Sample SNPDD}
        \begin{figure}[H]
            \centering
            \includegraphics[scale=0.5]{sample5}
            \caption{Sample SNPDD at step $k = 5$}
            \label{fig:sample5}
        \end{figure}
    \end{frame}

    \begin{frame}[shrink=0]
        \frametitle{Sample SNPDD}
        \begin{figure}[H]
            \centering
            \includegraphics[scale=0.5]{sample6}
            \caption{Sample SNPDD at step $k = 6$}
            \label{fig:sample6}
        \end{figure}
    \end{frame}

    \begin{frame}[shrink=75]
        \frametitle{SAT, step 0}
        \begin{figure}[H] 
            \centering
            \includegraphics{dias/sat0.png}
            \caption{SAT at step 0}
            \label{fig:sat0}
        \end{figure}
    \end{frame}

    \begin{frame}[shrink=75]
        \frametitle{SAT, step 1}
        \begin{figure}[H] 
            \centering
            \includegraphics{dias/sat1.png}
            \caption{SAT at step 1}
            \label{fig:sat1}
        \end{figure}
    \end{frame}

    \begin{frame}[shrink=75]
        \frametitle{SAT, step 2}
        \begin{figure}[H] 
            \centering
            \includegraphics{dias/sat2.png}
            \caption{SAT at step 2}
            \label{fig:sat2}
        \end{figure}
    \end{frame}

    \begin{frame}[shrink=75]
        \frametitle{SAT, step 3}
        \begin{figure}[H] 
            \centering
            \includegraphics{dias/sat3.png}
            \caption{SAT at step 3}
            \label{fig:sat3}
        \end{figure}
    \end{frame}

    \begin{frame}[shrink=75]
        \frametitle{SAT, step 4}
        \begin{figure}[H] 
            \centering
            \includegraphics{dias/sat4.png}
            \caption{SAT at step 4}
            \label{fig:sat4}
        \end{figure}
    \end{frame}

    \begin{frame}[shrink=75]
        \frametitle{SAT, step 5}
        \begin{figure}[H] 
            \centering
            \includegraphics{dias/sat5.png}
            \caption{SAT at step 5}
            \label{fig:sat5}
        \end{figure}
    \end{frame}

    \begin{frame}[shrink=75]
        \frametitle{SAT, step 6}
        \begin{figure}[H] 
            \centering
            \includegraphics{dias/sat6.png}
            \caption{SAT at step 6}
            \label{fig:sat6}
        \end{figure}
    \end{frame}

    \begin{frame}[shrink=75]
        \frametitle{SAT, step 7}
        \begin{figure}[H] 
            \centering
            \includegraphics{dias/sat7.png}
            \caption{SAT at step 7}
            \label{fig:sat7}
        \end{figure}
    \end{frame}

    \begin{frame}[shrink=75]
        \frametitle{SAT, step 8}
        \begin{figure}[H] 
            \centering
            \includegraphics{dias/sat8.png}
            \caption{SAT at step 8}
            \label{fig:sat8}
        \end{figure}
    \end{frame}

    \begin{frame}[shrink=75]
        \frametitle{SAT, step 9}
        \begin{figure}[H] 
            \centering
            \includegraphics{dias/sat9.png}
            \caption{SAT at step 9}
            \label{fig:sat9}
        \end{figure}
    \end{frame}

    \subsection {Vectors and Matrices}

    \begin{frame}
        \frametitle{Config Label Vector}
        \begin{defn}[\textbf{Config Label Vector}]
            The \textit{config label vector} $CLV^k$ is defined as $ CLV^k = \{clv^k_1, clv^k_2, ..., clv^k_m \}   $
            is a vector of size $m$ where 
            \begin{equation}
                clv^k_j =
                \begin{cases}
                    h & \text{if a neuron with the label $h \in H$ is present in the system under index $j$}\\
                    -1 & \text{if there is no neuron assigned to index $j$}
                \end{cases}
            \end{equation}
            The order of the labels is arbitrarily chosen, for convenience of checking.
            If a division or dissolution rule is applied, the config label vector will be updated
            to reflect changes.\\
            The $CLV$ of Figure \ref{fig:sample4} is $CLV^4 = \langle 3, 4, 1, 2 \rangle$.
        \end{defn}
    \end{frame}

    \begin{frame}
        \frametitle{Config Vector}
        \begin{defn}[\textbf {Configuration Vectors}]
        A \textit{configuration vector} $CV^k = \langle c^k_1,c^k_2, ..., c^k_m \rangle$
        shows the amount of spikes in a neuron where each $c_j, j=1,2,...,m$ is the amount of spikes in $\sigma_j$\\
        The $CV$ of Figure \ref{fig:sample4} is $CV^4 = \langle 0, 0, 1, 0 \rangle.$
        \end{defn}
    \end{frame}

    \begin{frame}
        \frametitle{Spiking Vector}
            \begin{defn}[\textbf {Spiking Vectors}]
            A \textit{spiking vector} $SV^k = \langle sv^k_1,...,sv^k_n \rangle$ corresponds to a $CV^k$ such that
            \begin{enumerate}
                \item
                    for each $i$ $(1\leq i \leq n), SV_{k_{(i)}} = 1$ whenever rule $r_i$ is applicable to 
                    $CV^k$, otherwise $sv^k_i = 0$
            \end{enumerate}
            The $SV$ of Figure \ref{fig:sample4} is $SV^4 = \langle 0, 0 , 1, 0 \rangle$
            \end{defn}
    \end{frame}

    \begin{frame}
        \begin{defn}[\textbf{Mask Vector}]
            Given $n$ rules present on the system at the $kth$ step,
            a Mask Vector $MV^{(k)}$ is defined as 
            $MV^{(k)} = \{ mv^k_1, mv^k_2, ..., mv^k_n \}$ 
            where 
            \begin{equation}
            mv^{k}_i =     
            \begin{cases}
                -1 & \text{if the rule is a dissolution rule} \\
                0 & \text{if the rule is either a spiking or forgetting rule} \\ 
                1 & \text{if the rule is a division rule}
            \end{cases}
            \end{equation} 
            Whenever a new spiking vector is computed, its individual elements are multiplied to the mask vector
            to obtain a new vector dictating which activated rules are either division or dissolution rules.\\
            The $MV$ of Figure \ref{fig:sample4} is $MV^4 = \langle 0, -1, 0, 0 \rangle$.
        \end{defn}
    \end{frame}

    \begin{frame}
        \begin{defn}[\textbf{Status Vector}]
        The vector $StV^k = \langle st^k_1,...,st^k_m \rangle$ is the status vector at the $kth$ step for each neuron represented
        by $st^k_j$ where
        \begin{equation}
            st^k_j =
            \begin{cases}
                1 & \text{if $\sigma_j$ is open}\\
                0 & \text{if $\sigma_j$ is closed}
            \end{cases}
        \end{equation}
        The $StV$ of Figure \ref{fig:sample4} is $StV^4 = \langle 1, 1, 1, 1 \rangle$
        \end{defn}
    \end{frame}

    \begin{frame}
        \frametitle{Rule Representation}
        \begin{defn}[\textbf{Rule Representation}]
        % rulerep: {j, k, sigma, d', consumed, form, local sigma, id}
        The set $R^k = r_1, r_2,...r_n$ contains the rules present in the system at time step $k$,
        and $r_i$ has been redefined into the following construct:
        $$ r_i = \{E, i_\circ, j, j_\circ, d', c\}$$ 
        where $E$, is the regular expression use in rule $i, j$ ,$i_\circ$
        is the rule's index for the rule dictionary $R$, $j_\circ$ is $j$'s label in $H$,
        $d'$ is the rule's fire status, and $c$ is the amount of spikes consumed.\\
        The $R$ of Figure \ref{fig:sample4} is $R = r_1, r_2, r_3$ where $r_1 = (a, 4, 1, 4, 0, 1),
        r_2 = (a, 2, 2, 1, 0, 1), r_3 = (a, 3, 3, 2, 0, 1)$.
        \end{defn}
    \end{frame}

    \begin{frame}
    \frametitle{Complete Rule Representation}
        \begin{defn}[\textbf{Complete Rule Representation}]
            The set $RC = r_1, r_2,...r_n$ includes all the rules that may appear in the system,
            where $r_i = \{E, j_\circ, c\}$.
            $E$ is the regular expression used in the rule, $j$ is the label of the neuron containing the rule,
            and $c$ is the number of spikes consumed.\\
            The $RC$ of \ref{fig:sample4} is $RC = r_1, r_2, r_3, r_4$ where
            $ r_1 = (a, 0, 1), r_2 = (a, 1, 1), r_3 = (a, 2, 1), r_4 = (a, 4, 1)  $
        \end{defn}
    \end{frame}

    \begin{frame}
    \frametitle{Delay Vector}
    \begin{defn}[\textbf {Delay Vector}]
    The delay vector $DV = \langle d_1,...,d_m \rangle$ stores the amount of delay for each rule $r_i, i = 1,...,n$ in $\Pi$ \\
    The $DV$ of Figure \ref{fig:sample4} is $DV = \langle 0, 0, 0 \rangle $.
    \end{defn}
    \end{frame}

    \begin{frame}[shrink=20]
        \frametitle{Removal Matrix}
            \begin{defn}[\textbf{Removal Matrix}]
            For a system composed of $m$ neurons and $n$ rules, the removal matrix $RM = \langle rm_1, rm_2,..., rm_n \rangle$
            indicates the spikes consumed by a neuron per rule where
            \begin{equation}
                rm_{i,j} =
                \begin{cases}
                    c & \text{if rule $r_i$ is contained in neuron $\sigma_j$ and consumes $c$ spikes to apply}\\
                    0 & \text{otherwise}
                \end{cases}
            \end{equation}
            The $RM$ in Figure \ref{fig:sample4} is 
            \begin{table}[]
                \centering
                \label{my-label}
                \begin{tabular}{|l|l|l|l|}
                \hline
                1 & 0 & 0 & 0 \\ \hline
                0 & 0 & 1 & 0 \\ \hline
                0 & 0 & 0 & 1 \\ \hline
                \end{tabular}
            \end{table}
            \end{defn}
    \end{frame}

    \begin{frame}
        \frametitle{Loss Vector}
        \begin{defn}[\textbf{Loss Vector}]
        The loss vector $LV^k = \langle lv_1,...,lv_m \rangle$ is the vector where for $lv_i$ for each neuron $\sigma_i, i =1,...,m$
        contains the number of spikes consumed of the rule in the specific neuron $\sigma_i$ at the $kth$ step.
        The loss vector can be obtained by multiplying $RM$ with the spiking vector.\\
        The $LV$ in Figure \ref{fig:sample4} is $LV^4 = \langle 0, 0, 1, 0 \rangle $
        \end{defn}
    \end{frame}

    \begin{frame}
        \frametitle{Gain Vector}
        \begin{defn}[\textbf{Gain Vector}]
        The gain vector $GV^k = \langle gv_1,...,gv_m \rangle$ is the gain vector which contains the total number of spikes gained,
        $gv_i$ for each neuron $\sigma_i, i =1,...,m$ at the $kth$ step, regardless if it is open or closed. \\
        The $GV$ in Figure \ref{fig:sample4} is $GV^4 = \langle 1, 1, 0, 1\rangle$ 
        \end{defn}
    \end{frame}
    
    \begin{frame}
        \frametitle{Spike Train Vector}
        \begin{defn}[\textbf{Spike Train Vector}]
            A spike train can be defined as the construct:
            $$spt_i = (d, h, a_1, a_2, ..., a_n)$$
            \begin{enumerate}
                \item $d$ is the delay before the spike train starts,
                \item $h$ is the label of the neuron it sends spikes to, and
                \item $a_1, a_2, ..., a_n$ are the spikes $h$ receives. 
            \end{enumerate}
        \end{defn}
        The $spt$ in Figure \ref{fig:sample0} is $spt_1 = (0, 1, 1)$
    \end{frame}

    \begin{frame}
        \frametitle{Spike Train Vector}
    \begin{defn}[\textbf{Spike Train Vector}]
        At time step $k = d$, $a_1$ spikes are received by $h$. 
        For every $m$th step after $k$, $h$ receives $a_{m+1}$ until $m+1 = n$.
        If neuron $h$ does not exist in the system yet, it will not receive spikes.
        The vector $SpT = \langle spt_1, spt_2, ... spt_t \rangle$ stores all $t$ spike trains.\\
        The \textit{Spike Train Vector} $SpTV^{(k)}$ is a vector which size is the same as the
        number of neurons present in the system $m$ and is defined as
        $SpTV^{(k)} = \langle sptv_1, sptv_2, ..., sptv_m \rangle$ where 
        \[
        sptv_j =
        \begin{cases}
            a_m & \text{neuron $h$ is in index $j$, and $h$} \\
                & \text{receives $a_m$ spikes from a spike train}\\
            0 & \text{otherwise}
        \end{cases}
        \]
        The $SpTV$ in Figure \ref{fig:sample0} is $SpTV = \langle 0, 0, 0 \rangle $.
        The $SpTV$ in Figure \ref{fig:sample1} is $SpTV = \langle 0, 1, 0 \rangle $
    \end{defn}
    \end{frame}

    \begin{frame}[shrink=10]
        \frametitle{Transition Matrix}
        \begin{defn}[\textbf{Transition Matrix}]
        The transition matrix of $\Pi$,
        is a set of vectors $TV$ defined as $TV = \{tv_1,....,tv_n\}$ where each $tv_i= \langle p_1,...,p_m \rangle$ and
        \end{defn}
        \begin{equation}
            p_j =
            \begin{cases}
                p & \text{if rule $r_i$ is in $\sigma_s$ and $ (s, j) \in syn)$ and it is applied producing $p$ spikes} \\
                0 & \text{otherwise}
            \end{cases}
        \end{equation}
        The $TM$ in Figure \ref{fig:sample4} is 
        \begin{table}[]
        \centering
        \label{my-label}
        \begin{tabular}{|l|l|l|l|}
        \hline
        0 & 0 & 0 & 0 \\ \hline
        1 & 1 & 0 & 1 \\ \hline
        0 & 0 & 1 & 0 \\ \hline
        \end{tabular}
        \end{table}
    \end{frame}
    
    \begin{frame}
        \frametitle{Indicator Vector}
            \begin{defn}[\textbf{Indicator Vector}]
            The indicator vector $IV^k = \langle iv_1,...,iv_m \rangle$ will be mupltiplied to the transition vector
            to obtain the net number of spikes a neuron receives, regardless if it's open or closed.\\
            The $IV$ of Figure \ref{fig:sample4} is $IV^4 = \langle 1, 1, 1, 1 \rangle$
            \end{defn}
    \end{frame}

    \begin{frame}
        \frametitle{Net Gain Vector}
        \begin{defn}[\textbf{Net Gain Vector}]
        The net gain vector $NG^k = GV^k \otimes st^k - LV^k$ at step $k$ where $\otimes$ is the element-wise
        multiplication operator.\\
        The $NG$ of Figure \ref{fig:sample4} is $NG^4 = \langle 1, 1, -1, 1 \rangle$.
        \end{defn}
    \end{frame}

\section{Algorithm Outline}
    \subsection{Algorithm for solving SNPDD}
    \begin{frame}
        \frametitle{General Algorithm for solving SNPDDs}
        Given the inputs
        $ CV^k, CLV^k, TM, RM, R, StV^k, MV^k $
        at the time step $k$:
        \begin{enumerate}
            \item Obtain the Spiking Vector $SV^k$
            \item Obtain the Spike Train Vector $SpTV^k$
            \item Elementwise-multiply $SV^k$ with the Mask Vector $MV^k$ to get only the applicable division and dissolution rules
            \item Resolve division and dissolution rule operations
            \item For all rules in $R$, compute for the Indicator Vector $IV^k$
            \item Compute for the Gain Vector $GV^k = SpTV^k + (IV^k * TV)$
            \item Compute for the Loss Vector $LV^k = SV^k * RM$
            \item Compute for the Net Gain Vector $NG^k = St^k * GV^k - LV^k$ and add it to $C$
            \item Decrement values of $d'$ in all $r_i$ in $R$ by 1 if not equal to -1;
        \end{enumerate}
    \end{frame}

    \subsection{Algorithm for acquiring the Spike Train Vector}

    \begin{frame}
        \frametitle{Acquiring SpTV}
        At the time step $k$:
        \begin{enumerate}
            \item For each label in the Config Label Vector $CLV$ in the index $j$, check if a spike train $spt_h$ exists for that label
            \item If $spt_h$ is ready to fire ($d \geq k$), add the number of spikes
                it sends to $SpTV_j$ and remove it from $spt_h$\\ (Meaning if a spike train sends $\alpha_1, \alpha_2,..., \alpha_n)$,
                send $\alpha_1$ then remove it so that the next time it sends a spike, it sends $\alpha_2$ and so on.)
        \end{enumerate}
    \end{frame}

    \subsection{Algorithms for Division and Dissolution}
    \begin{frame}
        \frametitle{Resolving Division}
        Given that the division rule is at index $i'$ and the neuron utilizing the rule is at index $j'$:
        \begin{enumerate}
            \item Count the number of new rules to be added from both children
            \item Shift all values of vectors pertaining to neurons at index $j'$ to the right by 1
            \item If there are more than 0, shift all values of vectors pertaining to rules at index $i'$ to the right
                by the number of new rules - 1.
            \item Remove $r_{i'}$ from $R$
            \item Add the children's rules to $R$
            \item Recreate the TV and the RM
            \item Recreate the Mask Vector $MV$ and the Delay Vector $DV$
        \end{enumerate}
    \end{frame}

    \begin{frame}
        \frametitle{Resolving Dissolution}
        Given that the dissolution rule is at index $i'$ and the neuron utilizing the rule is at index $j'$
        \begin{enumerate}
            \item Set all values of vectors pertaining to neurons at index $j'$ to 0
            \item Set all values of vectors pertaining to rules at index $i'$ to 0
            \item Modify RM and TM to remove the spikes sent and consumed by rules owned by the dissolving neuron
            \item Remove all rules of the dissolving neuron from $R$.
        \end{enumerate}
    \end{frame}

\section{Experimentation}
    \subsection{Limitations of the Software}
    \begin{frame}
        \frametitle{Label Limitations}
        \begin{itemize}
            \item Config Label Vector is limited to integer labels
            \item Substitutes must be used
            \item Value of $-1$ in $CLV$ means no neuron/label
        \end{itemize}
    \end{frame}

    \subsection{Results}
    \begin{frame}
        \subsection{Results}
        \begin{itemize}
            \item Able to simulate SAT and Subset Sum SNPDDs defined by \cite{snpdd}
            \item Test input files have been prepared
        \end{itemize}
    \end{frame}

    \begin{frame}
        \frametitle{SAT Problem}
        Label Changes:
        \begin{itemize}
            \item $d$ to 4
            \item $in_{x_1}$ to 5
            \item $in_{x_2}$ to 6
            \item $C_{x_1}1$ to 7
            \item $C_{x_1}0$ to 8 
            \item $C_{x_2}1$ to 9 
            \item $C_{x_2}0$ to 10
            \item $o_1$ to 11
            \item $o_0$ to 12
            \item $o_{11}$ to 13
            \item $o_{10}$ to 14
            \item $o_{01}$ to 15
            \item $o_{00}$ to 16
        \end{itemize}
    \end{frame}

    \begin{frame}
        \frametitle{SAT Problem}
        \begin{itemize}
            \item Boolean formula: $(x \lor y) \land (\neg x)$
            \item Expected values: $x = 0, y = 1$
        \end{itemize}
    \includegraphics[scale=0.45]{satres1}
        \begin{itemize}
            \item Output neurons except for neuron with label $15$ dissolved
            \item $15$ corresponds to $o_{01}$
            \item $x = 0, y = 1$
        \end{itemize}
    \end{frame}

    \begin{frame}
        \frametitle{SAT Problem}
        \begin{itemize}
            \item Boolean formula: $(\neg x \lor y) \land (x \lor \neg y)$
            \item Expected values: no possible values
        \end{itemize}
    \includegraphics[scale=0.45]{satres2}
        \begin{itemize}
            \item No output neurons remain!
        \end{itemize}
    \end{frame}

    \begin{frame}
        \frametitle{SAT Problem}
        \begin{itemize}
            \item Boolean formula: $(x \lor y) \land ( x)$
            \item Expected values: $x = 1, y = 1, 0$
        \end{itemize}
    \includegraphics[scale=0.45]{satres2}
        \begin{itemize}
            \item Output neurons 13 and 14 remain. 
            \item 13 corresponds to label $o_{11}$, 14 corresponds to label $o_{10}$
            \item $x = 1, y = 1$
            \item $x = 1, y = 0$
        \end{itemize}
    \end{frame}

    \begin{frame}
        \frametitle{Subset Sum}
        Label changes:
        \begin{itemize}
            \item $in_1$ to 5
            \item $in_2$ to 6
            \item $in_3$ to 7
            \item $d_{11}$ to 8
            \item $d_{12}$ to 9
            \item $d_{21}$ to 10
            \item $d_{22}$ to 11
            \item $d_{31}$ to 12
            \item $d_{32}$ to 13
            \item $o_1$ to 14
            \item $o_0$ to 15
        \end{itemize}
    \end{frame}

    \begin{frame}
        \frametitle{Subset Sum}
        Label changes:
        \begin{itemize}
            \item $o_{11}$ to 16
            \item $o_{10}$ to 17
            \item $o_{01}$ to 18
            \item $o_{00}$ to 19
            \item $o_{111}$ to 20
            \item $o_{110}$ to 21
            \item $o_{101}$ to 22
            \item $o_{100}$ to 23
            \item $o_{011}$ to 24
            \item $o_{010}$ to 25
            \item $o_{001}$ to 26
            \item $o_{000}$ to 27
            \item $s$ to 28
        \end{itemize}
    \end{frame}

    \begin{frame}
        \frametitle{Subset Sum}
        \begin{itemize}
            \item $ X = (1, 2, 3) $
            \item $ S = 5 $
            \item Expected result: $X' = (2, 3)$
        \end{itemize}
        \centering
        \includegraphics[scale=0.45]{sumres1}
        \begin{itemize}
            \item Output neuron remaining: 24
            \item 24 corresponds to label $ o_{011} $
            \item 1 is not included, 2 and 3 are included
        \end{itemize}
    \end{frame}

    \begin{frame}
        \frametitle{Subset Sum}
        \begin{itemize}
            \item $ X = (3, 2, 1) $
            \item $ S = 5 $
            \item Expected result: $X' = (3, 2)$
        \end{itemize}
        \centering
        \includegraphics[scale=0.45]{sumres2}
        \begin{itemize}
            \item Output neuron remaining: 21
            \item 21 corresponds to label $ o_{110} $
            \item 3 and 2 included, 1 not included
        \end{itemize}
    \end{frame}

    \begin{frame}
        \frametitle{Subset Sum}
        \begin{itemize}
            \item $ X = (2, 5, 8) $
            \item $ S = 15 $
            \item Expected result: $X' = (2, 5, 8)$
        \end{itemize}
        \centering
        \includegraphics[scale=0.45]{sumres3}
        \begin{itemize}
            \item Output neuron remaining: 20
            \item 20 corresponds to label $ o_{111} $
            \item The set itself is the subset satisfying the problem
        \end{itemize}
    \end{frame}

\section{Conclusion}
    \subsection{Conclusion}
    \begin{frame}
        \frametitle{Summary}
        \begin{itemize}
            \item A construct for spike trains was formed
            \item New vectors were formulated: the config label vector, the mask train vector, and the config label vector
            \item The existing algorithm for SNPs was appended to allow simulation of SNPs with Division and Dissolution
        \end{itemize}
    \end{frame}

    \subsection{Prospects}
    \begin{frame}
        \frametitle{In the future}
        \begin{itemize}
            \item Optimization of the algorithm
            \item Implementation of the program on CUDA
        \end{itemize}
    \end{frame}

    \subsection{Thanks}
    \begin{frame}
        \centering
        Thank you for listening!
    \end{frame}

\section{Bibliography}
\begin{frame}[shrink=40]
\frametitle{References}
\nocite{*}
\bibliography{refs}
\bibliographystyle{apalike}
\end{frame}

\end{document}
