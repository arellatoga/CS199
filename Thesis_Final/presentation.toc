\beamer@endinputifotherversion {3.36pt}
\select@language {english}
\beamer@sectionintoc {1}{Theoretical Framework}{3}{0}{1}
\beamer@sectionintoc {2}{Statement of the Problem}{23}{0}{2}
\beamer@sectionintoc {3}{Significance of the Study}{25}{0}{3}
\beamer@sectionintoc {4}{Objectives of the Study}{28}{0}{4}
\beamer@sectionintoc {5}{Scope and Limitations of the Study}{30}{0}{5}
\beamer@sectionintoc {6}{Research Plan}{33}{0}{6}
\beamer@sectionintoc {7}{Improvised Algorithm}{34}{0}{7}
\beamer@sectionintoc {8}{Software Implementation}{38}{0}{8}
\beamer@subsectionintoc {8}{1}{Technical implementation}{38}{0}{8}
\beamer@subsectionintoc {8}{2}{Software specifications}{39}{0}{8}
\beamer@sectionintoc {9}{Closing Remarks}{40}{0}{9}
\beamer@sectionintoc {10}{Bibliography}{41}{0}{10}
