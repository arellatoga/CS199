1) Remove "simplest"
Removed word

2) Lowercase P_1 and P_2
Changed to lowercase

3) Itemize in and out
Fixed placement of \item

4) Explanation of spike trains
Added explanation

5) Figure 1: SAT in Related Work
Replaced by old 3-neuron example, updated the definitions to reflect
the vector representations

6) What is m and n?
Defined m and n in the definitions

7) Vague description of Status Vector
Updated to be more concise

8) Figure ?? in Delay Vector definition
Fixed to show number

9) Vague description of Transition Matrix
Re-worded the definition

10) Remove "simple" word
Removed word

11) "But SNPDD use exponential labels etc at beginning"
Consult tomorrow for clarifications

12) Order of the CLV 
Added explanation on the order of labels

13) Ambiguous Spike Trains
Consult tomorrow, presentation is the same as those by Zhao et al

14) Explain in stages
Updated explanations

15) What ordering used
Added explanation on the ordering (see no 12)

16) Circles over the boolean formulas
Removed boolean formulas

17) Spike trains still present
Removed spike trains from diagrams

18) Sample i/o
To be added later

19) Incomplete citations
Will update refs.bib when internet is cooperative
