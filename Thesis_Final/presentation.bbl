\begin{thebibliography}{}

\bibitem[Cabarle et~al., 2012]{gpusim}
Cabarle, F., Adorna, H., Martinez-del amor, M., and Perez-Jimenez, M. (2012).
\newblock {\textsc Improving GPU Simulations of Spiking Neural P Systems}.
\newblock {\em Romanian Journal of Information}, 15:5--20.

\bibitem[Carandang, 2017]{regex}
Carandang, J. (2017).
\newblock {\textsc Improvements on CuSNP: A Parallel Simulator for Spiking
  Neural System}.

\bibitem[Carandang et~al., ]{cusnp}
Carandang, J., Villafores, J., Cabarle, F., Adorna, H., and Martinez-del amor,
  M.
\newblock {\textsc CuSNP: Spiking Neural P Systems Simulators in CUDA}.
\newblock {\em Romanian Journal of Information}.

\bibitem[Ionescu et~al., 2006]{snp}
Ionescu, M., P\u{a}un, G., and Yokomori, T. (2006).
\newblock {\textsc Spiking Neural P Systems}.
\newblock {\em Fundamenta Informaticae}, 71:279--308.

\bibitem[Pan et~al., ]{snpdb}
Pan, L., Paun, G., and Perez-Jimenez, M.
\newblock {\textsc Spiking Neural P Systems with Neuron Division and Budding}.

\bibitem[Paun, 2010]{memcomp}
Paun, G. (2010).
\newblock {\textsc Membrane Computing}.
\newblock {\em Scholarpedia}, 5(1):9259.
\newblock {revision \#91480}.

\bibitem[Paun et~al., 2009]{oxford}
Paun, G., Rosenburg, G., and Salomaa, A. (2009).
\newblock {\em {\textsc The Oxford Handbook of Membrane Computing}}.
\newblock Oxford University Press.

\bibitem[P\u{a}un, 2007]{snp-tutorial}
P\u{a}un, G. (2007).
\newblock {\textsc Spiking Neural P Systems. A Tutorial}.
\newblock {\em Bulletin of the European Association for Theoretical Computer
  Science}, 91:145--159.

\bibitem[Sipser, ]{sipser}
Sipser, M.
\newblock {\em {\textsc Introduction to the Theory of Computation}}.

\bibitem[Zhao et~al., 2016]{snpdd}
Zhao, Y., Liu, X., and Wang, W. (2016).
\newblock {\textsc Spiking Neural P Systems with Neuron Division and
  Dissolution}.

\end{thebibliography}
