\select@language {english}
\select@language {english}
\contentsline {section}{\numberline {1}Background of the Study}{1}{section.1}
\contentsline {section}{\numberline {2}Theoretical Framework}{1}{section.2}
\contentsline {section}{\numberline {3}Related work}{3}{section.3}
\contentsline {section}{\numberline {4}Problem Statement}{3}{section.4}
\contentsline {section}{\numberline {5}Significance of the study}{3}{section.5}
\contentsline {section}{\numberline {6}Objectives of the Study}{4}{section.6}
\contentsline {section}{\numberline {7}Scope and Limitations of the Study}{4}{section.7}
\contentsline {section}{\numberline {8}Research plan}{4}{section.8}
\contentsline {subsection}{\numberline {8.1}Methodology}{4}{subsection.8.1}
\contentsline {subsection}{\numberline {8.2}Timetable}{5}{subsection.8.2}
\contentsline {subsection}{\numberline {8.3}Research outputs}{5}{subsection.8.3}
\contentsline {subsection}{\numberline {8.4}A sample table}{5}{subsection.8.4}
