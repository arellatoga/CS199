\select@language {english}
\contentsline {section}{\numberline {1}Background of the Study}{1}
\contentsline {section}{\numberline {2}Theoretical Framework}{1}
\contentsline {section}{\numberline {3}Related Work}{2}
\contentsline {subsection}{\numberline {3.1}Definitions}{2}
\contentsline {subsection}{\numberline {3.2}Hardware aspects}{5}
\contentsline {subsection}{\numberline {3.3}Software aspects}{5}
\contentsline {section}{\numberline {4}Statement of the Problem}{6}
\contentsline {section}{\numberline {5}Significance of the Study}{6}
\contentsline {section}{\numberline {6}Objectives of the Study}{6}
\contentsline {section}{\numberline {7}Scope and Limitations of the Study}{6}
\contentsline {section}{\numberline {8}Research Plan}{7}
\contentsline {subsection}{\numberline {8.1}Methodology}{7}
\contentsline {subsection}{\numberline {8.2}Timetable}{7}
\contentsline {subsection}{\numberline {8.3}Research Outputs}{7}
